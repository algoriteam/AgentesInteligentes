/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behavious_Example;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;


public class SendPlayerInfo extends TickerBehaviour{
    
    private String content;
    
    public SendPlayerInfo(String content, Agent my, long time){
        super(my, time);
        this.content = content;
    }

    @Override
    public void onTick() {
        AID receiver = new AID();
        receiver.setLocalName("petri");

        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        long time = System.currentTimeMillis();
        msg.setConversationId(""+time);
        msg.addReceiver(receiver);
        msg.setContent(content);

        myAgent.send(msg);
    }
        
}