/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behavious_Example;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;


public class ReceiveRequest extends CyclicBehaviour {
   
    @Override
    public void action(){
        ACLMessage msg = myAgent.receive();
        if (msg != null){
            ACLMessage resp = msg.createReply();
            System.out.println("Recebi uma mensagem de " + msg.getSender() + ". Conteúdo: " + msg.getContent());
            if(msg.getPerformative() == 11){
                resp.setContent("Yes");
                resp.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
            }
            else{
                resp.setContent("No");
                resp.setPerformative(ACLMessage.NOT_UNDERSTOOD);
            }
            
            myAgent.send(resp);
        }
        block();
    }
}
