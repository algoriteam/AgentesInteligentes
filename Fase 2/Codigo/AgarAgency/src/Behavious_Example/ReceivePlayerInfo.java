/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behavious_Example;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;


public class ReceivePlayerInfo extends CyclicBehaviour {
   
    @Override
    public void action(){
        ACLMessage msg = myAgent.receive();
        if (msg != null){
            
            if(msg.getPerformative() == 0){
                System.out.println("Recebi uma mensage de " + msg.getSender() + ". Conteúdo: Ok!");
            }
            else{
                System.out.println("Recebi uma mensage de " + msg.getSender() + ". Conteúdo: Not OK");
            }
        }
        block();
    }
}
