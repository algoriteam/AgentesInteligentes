package Behaviours;

import Data.Player;
import Data.PlayerScoreComparator;
import Packages.LeadPlayers;
import Presentation.GameMaster;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class SendLeadPlayers extends TickerBehaviour{
    
    // ----- VARIABLES ----- //
    
            private GameMaster gm;
    
    // ----- CONSTRUCTORS ----- //
    
    public SendLeadPlayers(Agent a, long time, GameMaster m){
        super(a, time);
        this.gm = m;
    }
    
    // ----- METHODS ----- //

    @Override
    protected void onTick() {
        synchronized(this.gm.getTable().getPlayersPointer()){
            LeadPlayers n = new LeadPlayers();
            
            // CALCULATE TOP 10 or less PLAYERS
            // Sort players for score value
            ArrayList<Player> players_list = new ArrayList<>(this.gm.getTable().getPlayersPointer().values());
            Collections.sort(players_list, new PlayerScoreComparator());
            
            // Fecth top 10 or less
            Iterator<Player> it_leaders = players_list.iterator();
            int i;
            for(i = 0; i < 10 && it_leaders.hasNext(); i++){
                Player p = it_leaders.next();
                n.addLead(p);
            }
        
            Iterator<String> it_players = this.gm.getTable().getPlayersPointer().keySet().iterator();
            while(it_players.hasNext()){
                String name = it_players.next();
                Player p = this.gm.getTable().getPlayersPointer().get(name);
                
                if(!p.isAgent()){
                    try{
                        // Set receiver
                        AID receiver = new AID();
                        receiver.setLocalName(name);

                        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);

                        // Set message content
                        msg.addReceiver(receiver);
                        msg.setConversationId("35");
                        msg.setContentObject(n);

                        // Send message
                        myAgent.send(msg);
                    }
                    catch(IOException e){
                        System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' couldn't send near players!");
                    }
                }
                
            }
        }
        
    }
        
}
