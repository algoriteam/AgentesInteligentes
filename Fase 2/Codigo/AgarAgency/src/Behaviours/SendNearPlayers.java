package Behaviours;

import Data.Player;
import Data.Virus;
import Packages.NearPlayers;
import Packages.NearVirus;
import Presentation.GameMaster;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.util.Iterator;

public class SendNearPlayers extends TickerBehaviour{
    
    // ----- VARIABLES ----- //
    
            private GameMaster gm;
    
    // ----- CONSTRUCTORS ----- //
    
    public SendNearPlayers(Agent a, long time, GameMaster m){
        super(a, time);
        this.gm = m;
    }
    
    // ----- METHODS ----- //

    @Override
    protected void onTick() {
        synchronized(this.gm.getTable().getPlayersPointer()){
            Iterator<String> it_players1 = this.gm.getTable().getPlayersPointer().keySet().iterator();
            while(it_players1.hasNext()){
                String name1 = it_players1.next();
                Player p1 = this.gm.getTable().getPlayersPointer().get(name1);
                NearPlayers n = new NearPlayers();
                NearVirus v = new NearVirus();
                
                // Calculate nearest Virus
                Iterator<String> it_virus = this.gm.getTable().getVirusPointer().keySet().iterator();
                while(it_virus.hasNext()){
                    String key = it_virus.next();
                    Virus vi = this.gm.getTable().getVirusPointer().get(key);
                    if(p1.sights(vi, p1.getRange())){
                        v.addNear(vi);
                    }
                }
                
                // Calculate nearest Players
                Iterator<String> it_players2 = this.gm.getTable().getPlayersPointer().keySet().iterator();
                while(it_players2.hasNext()){
                    String name2 = it_players2.next();
                    Player p2 = this.gm.getTable().getPlayersPointer().get(name2);
                    if(!name1.equals(name2) && p1.sights(p2, p1.getRange()) && !v.hasPlayer(p2)){
                        n.addNear(p2);
                    }
                }
                
                try{
                    // Set receiver
                    AID receiver = new AID();
                    receiver.setLocalName(name1);
                    receiver.addAddresses("http://192.168.1.81:7778/acc");
                    
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    
                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("31");
                    msg.setContentObject(n);

                    // Send message
                    myAgent.send(msg);
                }
                catch(IOException e){
                    System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' couldn't send near players!");
                }   
            }
        }
    }
    
}
