package Behaviours;

import Data.Player;
import Packages.SimplePlayer;
import Presentation.GameMaster;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public class PlayerManagement extends CyclicBehaviour{
    
    // ----- VARIABLES ----- //
    
            private GameMaster gm;
    
    // ----- CONSTRUCTORS ----- //
    
    public PlayerManagement(GameMaster m){
        this.gm = m;
    }
    
    // ----- METHODS ----- //
    
    @Override
    public void action() {
        ACLMessage request = myAgent.receive();
        if (request != null){
            try {
                switch(request.getConversationId()){
                    // UPDATE_POSITION
                    case "34":
                        synchronized(this.gm.getTable().getPlayersPointer()){
                            SimplePlayer pos = (SimplePlayer) request.getContentObject();
                            Player p = this.gm.getTable().getPlayersPointer().get(pos.getName());
                            if(p != null){
                                p.setPosX(pos.getPosX());
                                p.setPosY(pos.getPosY());
                                p.setScore(pos.getScore());
                                p.setType(pos.getType());
                            }
                        }
                        break;
                    // REMOVE_PLAYER
                    case "32":
                        String key = (String) request.getContentObject();
                        this.gm.getTable().removePlayer(key);
                        
                        // Warn player that he died
                        
                        // Set receiver
                        AID receiver = new AID();
                        receiver.setLocalName(key);

                        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);

                        // Set message content
                        msg.addReceiver(receiver);
                        msg.setConversationId("33");

                        // Send message
                        myAgent.send(msg);
                        break;
                    default:
                        System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' received an invalid id package");
                        break;
                }
            } 
            catch (UnreadableException ex) {
                System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' received an unreadable package");
            }
        }
    }
        
}
