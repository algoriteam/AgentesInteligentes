package Behaviours;

import Data.Aglomerado;
import Data.Player;
import Packages.NearAglomerados;
import Presentation.GameMaster;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.util.Iterator;

public class SendNearAglomerados extends TickerBehaviour{
    
    // ----- VARIABLES ----- //
    
            private GameMaster gm;
    
    // ----- CONSTRUCTORS ----- //
    
    public SendNearAglomerados(Agent a, long time, GameMaster m){
        super(a, time);
        this.gm = m;
    }
    
    // ----- METHODS ----- //

    @Override
    protected void onTick() {
        synchronized(this.gm.getTable().getPlayersPointer()){
            Iterator<String> it_players = this.gm.getTable().getPlayersPointer().keySet().iterator();
            while(it_players.hasNext()){
                String name = it_players.next();
                Player p = this.gm.getTable().getPlayersPointer().get(name);
                NearAglomerados n = new NearAglomerados();
                
                synchronized(this.gm.getTable().getAglomeradosPointer()){           
                    // Calculate nearest Aglomerados
                    Iterator<String> it_aglomerados = this.gm.getTable().getAglomeradosPointer().keySet().iterator();
                    while(it_aglomerados.hasNext()){
                        String key = it_aglomerados.next();
                        Aglomerado a = this.gm.getTable().getAglomeradosPointer().get(key);
                        if(p.sights(a, p.getRange())){
                            n.addNear(a);
                        }
                    }
                }
                
                try{
                    // Set receiver
                    AID receiver = new AID();
                    receiver.setLocalName(name);
                    
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    
                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("11");
                    msg.setContentObject(n);

                    // Send message
                    myAgent.send(msg);
                }
                catch(IOException e){
                    System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' couldn't send near aglomerados");
                }    
            }
        }
    }
        
}
