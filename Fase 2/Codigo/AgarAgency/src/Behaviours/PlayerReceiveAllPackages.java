package Behaviours;

import Data.Player;
import Packages.LeadPlayers;
import Packages.NearAglomerados;
import Packages.NearPlayers;
import Packages.NearVirus;
import Presentation.GameMaster;
import Presentation.PlayerScreen;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public class PlayerReceiveAllPackages extends CyclicBehaviour{
    
    // ----- VARIABLES ----- //
    
            private GameMaster gm;
            
            private Player me;
            private PlayerScreen player_screen;
    
    // ----- CONSTRUCTORS ----- //
    
    public PlayerReceiveAllPackages(GameMaster m, PlayerScreen l, Player p){
        this.gm = m;
        
        this.me = p;
        this.player_screen = l;
    }
    
    // ----- METHODS ----- //
    
    @Override
    public void action() {
        ACLMessage answer = myAgent.receive();
        if (answer != null){
            try{
                switch(answer.getConversationId()){
                    // NEAR_AGLOMERADOS
                    case "11":
                        synchronized(me.getNearAglomeradosPointer()){
                            me.setNearAglomerados((NearAglomerados) answer.getContentObject());
                        }
                        break;
                    // NEAR_VIRUS
                    case "21":
                        synchronized(me.getNearVirusPointer()){
                            me.setNearVirus((NearVirus) answer.getContentObject());
                        }
                        break;
                    // NEAR_PLAYERS
                    case "31":
                        synchronized(me.getNearPlayersPointer()){
                            me.setNearPlayers((NearPlayers) answer.getContentObject());
                        }
                        break;
                    // KILL_PLAYER
                    case "33":
                        this.gm.getAgentsPlatform().removeAgentFromPlatform(me.getName());
                        this.player_screen.dispose();
                        break;
                    // LEAD_PLAYERS
                    case "35":
                        synchronized(me.getLeadPlayersPointer()){
                            me.setLeadPlayers((LeadPlayers) answer.getContentObject());
                        }
                        break;
                }
            }
            catch(UnreadableException e){
                System.out.println("[AGENTS] Agent '" + myAgent.getLocalName() + "' received an unreadable package");
            }
        }
    }
        
}
