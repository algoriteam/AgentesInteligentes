package Behaviours;

import Presentation.GameMaster;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public class AglomeradosManagement extends CyclicBehaviour{
    
    // ----- VARIABLES ----- //
    
            private GameMaster gm;
    
    // ----- CONSTRUCTORS ----- //
    
    public AglomeradosManagement(GameMaster m){
        this.gm = m;
    }
    
    // ----- METHODS ----- //
    
    @Override
    public void action() {
        ACLMessage request = myAgent.receive();
        if (request != null){
            try {
                switch(request.getConversationId()){
                    case "12":
                        String key = (String) request.getContentObject();
                        this.gm.getTable().removeAglomerado(key);
                        break;
                    default:
                        System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' received an invalid id package");
                        break;
                }
            } 
            catch (UnreadableException ex) {
                System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' received an unreadable package");
            }
        }
    }
        
}
