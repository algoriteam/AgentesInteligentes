package Behaviours;

import Data.Player;
import Packages.SimplePlayer;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;

public class SendPlayerStatus extends TickerBehaviour{
    
    // ----- VARIABLES ----- //
    
    private Player me;
    
    // ----- CONSTRUCTORS ----- //
    
    public SendPlayerStatus(Agent a, long time, Player c){
        super(a, time);
        this.me = c;
    }
    
    // ----- METHODS ----- //

    @Override
    protected void onTick() {
        try{
            // Set receiver
            AID receiver = new AID();
            receiver.setLocalName("service_players");

            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);

            // Build package
            SimplePlayer pos = new SimplePlayer(this.me);

            // Set message content
            msg.addReceiver(receiver);
            msg.setConversationId("34");
            msg.setContentObject(pos);

            // Send message
            myAgent.send(msg);
        }
        catch(IOException e){
            System.out.println("[AGENTS] Agent '" + myAgent.getLocalName() + "' couldn't send his new status!");
        }
        
    }
        
}
