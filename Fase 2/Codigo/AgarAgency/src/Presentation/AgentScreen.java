package Presentation;

import Controllers.Agent_Controller;
import Data.Player;
import jade.gui.GuiAgent;
import java.awt.Color;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

public class AgentScreen extends javax.swing.JFrame {

    // ----- VARIABLES ----- //

        // GameMaster
        private GameMaster gm;
        
        // Player controller
        private Agent_Controller controller;

        // The player
        private Player me;
        private String last_status;
        
        // The agent
        private GuiAgent agent;
    
    // ----- CONSTRUCTORS ----- //
    
    public AgentScreen(GameMaster m, GuiAgent a, Player p) {
        initComponents();
        
        this.gm = m;
        this.agent = a;
        this.me = p;
        this.last_status = "";
        this.controller = new Agent_Controller(this.gm, this, this.agent, this.me);
        this.game_frame.setContentPane(this.controller);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setTitle("AgarAgency - " + p.getName());
        
        this.setAgent_name(this.me.getName());
        this.setAgent_type(this.me.getType());
    }

    public void setAgent_name(String name) {
        this.Agent_name.setText(name);
    }

    public void setAgent_score(String score) {
        this.Agent_score.setText(score);
    }
    
    public void setAgent_type(int type) {
        this.Agent_Type.setSelectedIndex(type);
    }

    public void setAgent_status(int status) {
        String current_status = "";
        switch(status){
            case 0:
                current_status = "This looks like the desert";
                this.Agent_status.setBackground(new Color(220, 220, 220));
                break;
            case 1:
                current_status = "Found some food!";
                this.Agent_status.setBackground(new Color(147, 200, 255));
                break;
            case 2:
                current_status = "Someone is after me";
                this.Agent_status.setBackground(new Color(255, 137, 137));
                break;
            case 3:
                current_status = "I found someone to eat";
                this.Agent_status.setBackground(new Color(255, 200, 137));
                break;
            case 4:
                current_status = "Keeping a safe distance from a virus";
                this.Agent_status.setBackground(new Color(255, 137, 137));
                break;
            case 5:
                current_status = "Im hiding in a nearby virus";
                this.Agent_status.setBackground(new Color(137, 255, 143));
                break;
            case 6:
                current_status = "Damn, i got stuck in a virus";
                this.Agent_status.setBackground(new Color(255, 137, 137));
                break;
            case 7:
                current_status = "I ate someone and it was delicious";
                this.Agent_status.setBackground(new Color(137, 255, 143));
                break;
            case 8:
                current_status = "Uppp uppp hooorrrayy!";
                this.Agent_status.setBackground(Color.BLUE);
                break;
        }
        if(!this.last_status.equals(current_status)){
            this.me.addLog(current_status);
            this.last_status = current_status;
        }
        this.Agent_status.setText(current_status);
    }
    
    public void setAgent_log(String log) {
        this.Agent_log.setText(log);
    }
    
    public void setFrame_counter(String fps) {
        this.Frame_counter.setText(fps);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        game_frame = new javax.swing.JInternalFrame();
        Button_Close = new javax.swing.JButton();
        Text_frame_counter = new javax.swing.JLabel();
        Frame_counter = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        Text_virus_counter2 = new javax.swing.JLabel();
        Text_agent_name = new javax.swing.JLabel();
        Agent_name = new javax.swing.JLabel();
        Text_agent_type = new javax.swing.JLabel();
        Text_agent_score = new javax.swing.JLabel();
        Agent_score = new javax.swing.JLabel();
        Text_virus_counter3 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        Agent_status = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Agent_log = new javax.swing.JTextArea();
        Agent_Type = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AgarAgency - GameMaster");
        setAlwaysOnTop(true);
        setResizable(false);

        game_frame.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        game_frame.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        game_frame.setResizable(true);
        game_frame.setTitle("Petri table");
        game_frame.setPreferredSize(new java.awt.Dimension(600, 600));
        game_frame.setVisible(true);

        javax.swing.GroupLayout game_frameLayout = new javax.swing.GroupLayout(game_frame.getContentPane());
        game_frame.getContentPane().setLayout(game_frameLayout);
        game_frameLayout.setHorizontalGroup(
            game_frameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 424, Short.MAX_VALUE)
        );
        game_frameLayout.setVerticalGroup(
            game_frameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 385, Short.MAX_VALUE)
        );

        Button_Close.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Button_Close.setText("Close");
        Button_Close.setMaximumSize(new java.awt.Dimension(117, 31));
        Button_Close.setMinimumSize(new java.awt.Dimension(117, 31));
        Button_Close.setPreferredSize(new java.awt.Dimension(117, 31));
        Button_Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Button_CloseActionPerformed(evt);
            }
        });

        Text_frame_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_frame_counter.setText("FPS:");

        Frame_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Frame_counter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Frame_counter.setText("NaN");

        Text_virus_counter2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Text_virus_counter2.setForeground(new java.awt.Color(153, 153, 153));
        Text_virus_counter2.setText("Agents");

        Text_agent_name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_agent_name.setForeground(new java.awt.Color(0, 0, 153));
        Text_agent_name.setText("Name:");

        Agent_name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Agent_name.setForeground(new java.awt.Color(0, 0, 153));
        Agent_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Agent_name.setText("NaN");

        Text_agent_type.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_agent_type.setForeground(new java.awt.Color(0, 0, 153));
        Text_agent_type.setText("Type:");

        Text_agent_score.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_agent_score.setForeground(new java.awt.Color(0, 0, 153));
        Text_agent_score.setText("Score:");

        Agent_score.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Agent_score.setForeground(new java.awt.Color(0, 0, 153));
        Agent_score.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Agent_score.setText("NaN");

        Text_virus_counter3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Text_virus_counter3.setForeground(new java.awt.Color(153, 153, 153));
        Text_virus_counter3.setText("Properties");

        Agent_status.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Agent_status.setForeground(new java.awt.Color(255, 255, 255));
        Agent_status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Agent_status.setText("What am i doing?");
        Agent_status.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));
        Agent_status.setOpaque(true);

        Agent_log.setColumns(20);
        Agent_log.setRows(5);
        jScrollPane1.setViewportView(Agent_log);

        Agent_Type.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Agent_Type.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Basic", "Eagle Eye", "Blind", "Chicken", "Panic", "Stealth" }));
        Agent_Type.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Agent_TypeItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(game_frame, javax.swing.GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
                    .addComponent(Agent_status, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Text_virus_counter3)
                            .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Text_agent_type)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Agent_Type, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Text_agent_score, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(Text_agent_name, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(Agent_score, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                                    .addComponent(Agent_name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Text_frame_counter)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Frame_counter))
                            .addComponent(Text_virus_counter2)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(Button_Close, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Frame_counter)
                            .addComponent(Text_frame_counter))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Text_virus_counter2)
                        .addGap(5, 5, 5)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Text_agent_name)
                            .addComponent(Agent_name))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Agent_score)
                            .addComponent(Text_agent_score))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Text_virus_counter3)
                        .addGap(5, 5, 5)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Text_agent_type)
                            .addComponent(Agent_Type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Button_Close, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(game_frame, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Agent_status, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
  
    private void Button_CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Button_CloseActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_Button_CloseActionPerformed

    private void Agent_TypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Agent_TypeItemStateChanged
        int index = this.Agent_Type.getSelectedIndex();
        if(index > -1){
            this.me.setType(index);
        }
    }//GEN-LAST:event_Agent_TypeItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Agent_Type;
    private javax.swing.JTextArea Agent_log;
    private javax.swing.JLabel Agent_name;
    private javax.swing.JLabel Agent_score;
    private javax.swing.JLabel Agent_status;
    private javax.swing.JButton Button_Close;
    private javax.swing.JLabel Frame_counter;
    private javax.swing.JLabel Text_agent_name;
    private javax.swing.JLabel Text_agent_score;
    private javax.swing.JLabel Text_agent_type;
    private javax.swing.JLabel Text_frame_counter;
    private javax.swing.JLabel Text_virus_counter2;
    private javax.swing.JLabel Text_virus_counter3;
    private javax.swing.JInternalFrame game_frame;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    // End of variables declaration//GEN-END:variables
}
