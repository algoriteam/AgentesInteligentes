package Presentation;

import Controllers.Player_Controller;
import Data.Player;
import jade.gui.GuiAgent;

public class PlayerScreen extends javax.swing.JFrame {

    // ----- VARIABLES ----- //
        
        // GameMaster
        private GameMaster gm;
        
        // Player controller
        private Player_Controller controller;

        // The player
        private Player me;
        
        // The agent
        private GuiAgent agent;
    
    // ----- CONSTRUCTORS ----- //
    
    public PlayerScreen(GameMaster m, GuiAgent a, Player p) {
        initComponents();
        
        this.gm = m;
        this.me = p;
        this.agent = a;
        
        this.controller = new Player_Controller(this.gm, this, this.agent, this.me);
        this.setContentPane(this.controller);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setTitle("AgarAgency - " + p.getName());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AgarAgency - Player");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 900, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 626, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
