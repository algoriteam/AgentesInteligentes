package Presentation;

import Data.Petri;
import Controllers.Master_Controller;
import Controllers.Platform_Controller;
import Data.Player;
import javax.swing.JLabel;

public class GameMaster extends javax.swing.JFrame {

    // ----- VARIABLES ----- //
        
        // Network settings
        private int messages_rate = 30; // Messages per second - NOTICE THAT THIS WILL BE THE AGENTS CALCULATION AND FRAME RATE
        private int message_tick = 1000/30; // Timelapse to send new message
        
        // Graphic settings
        private int tick_rate = 60; // Frames per second
        private int refresh_rate = 1000/60; // In milliseconds
        private volatile Boolean antialiasing = false;

        private int width = 2500;
        private int height = 2500;

        // Petri table
        private Petri table;
        
        private int max_aglomerados = 1000;
        private int max_virus = 10;
        
        private int new_player_aglomerados = 100;
        private int new_player_virus = 2;

        // Master controller
        private Master_Controller controller;
        
        // Agents platform
        private Platform_Controller agents_platform;
    
    // ----- CONSTRUCTORS ----- //
    
    public GameMaster() {
        initComponents();
        
        // Create Petri
        this.table = new Petri(this.width, this.height, this.max_aglomerados, this.max_virus, this.new_player_aglomerados, this.new_player_virus);
        
        // Create Controller
        this.controller = new Master_Controller(this);
        this.game_frame.setContentPane(this.controller);
        
        // Create Agents Platform
        this.agents_platform = new Platform_Controller();
        
        this.agents_platform.initMainContainer("localhost", "1099");
        
        // Create Services Agents
        this.agents_platform.startServiceInPlatform("service_players", this, "Services.Service_Players");
        this.agents_platform.startServiceInPlatform("service_virus", this, "Services.Service_Virus");
        this.agents_platform.startServiceInPlatform("service_aglomerados", this, "Services.Service_Aglomerados");
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        game_frame = new javax.swing.JInternalFrame();
        Button_Exit = new javax.swing.JButton();
        Button_Add_Player = new javax.swing.JButton();
        Text_frame_counter = new javax.swing.JLabel();
        Frame_counter = new javax.swing.JLabel();
        Text_player_counter = new javax.swing.JLabel();
        Player_counter = new javax.swing.JLabel();
        Text_virus_counter = new javax.swing.JLabel();
        Virus_counter = new javax.swing.JLabel();
        Text_aglomerados_counter = new javax.swing.JLabel();
        Aglomerados_counter = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        Text_virus_counter1 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        Text_virus_counter2 = new javax.swing.JLabel();
        Button_Add_Agent = new javax.swing.JButton();
        Text_agent_counter = new javax.swing.JLabel();
        Agents_counter = new javax.swing.JLabel();
        Text_virus_counter3 = new javax.swing.JLabel();
        Text_agent_name = new javax.swing.JLabel();
        Agent_name = new javax.swing.JLabel();
        Agent_type = new javax.swing.JLabel();
        Text_agent_type = new javax.swing.JLabel();
        Text_agent_score = new javax.swing.JLabel();
        Agent_score = new javax.swing.JLabel();
        Button_Show_Agent = new javax.swing.JButton();
        Button_Remove_Agent = new javax.swing.JButton();
        Check_Antialiasing = new javax.swing.JCheckBox();
        Tick_rate_value = new javax.swing.JSlider();
        Text_tick_rate_slider = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        Tick_rate = new javax.swing.JLabel();
        Text_refresh_rate = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AgarAgency - GameMaster");

        game_frame.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        game_frame.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        game_frame.setResizable(true);
        game_frame.setTitle("Petri table");
        game_frame.setPreferredSize(new java.awt.Dimension(600, 600));
        game_frame.setVisible(true);

        javax.swing.GroupLayout game_frameLayout = new javax.swing.GroupLayout(game_frame.getContentPane());
        game_frame.getContentPane().setLayout(game_frameLayout);
        game_frameLayout.setHorizontalGroup(
            game_frameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 568, Short.MAX_VALUE)
        );
        game_frameLayout.setVerticalGroup(
            game_frameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        Button_Exit.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Button_Exit.setText("Exit");
        Button_Exit.setMaximumSize(new java.awt.Dimension(117, 31));
        Button_Exit.setMinimumSize(new java.awt.Dimension(117, 31));
        Button_Exit.setPreferredSize(new java.awt.Dimension(117, 31));
        Button_Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Button_ExitActionPerformed(evt);
            }
        });

        Button_Add_Player.setBackground(new java.awt.Color(0, 51, 204));
        Button_Add_Player.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Button_Add_Player.setText("Add player");
        Button_Add_Player.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Button_Add_PlayerActionPerformed(evt);
            }
        });

        Text_frame_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_frame_counter.setText("FPS:");

        Frame_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Frame_counter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Frame_counter.setText("NaN");

        Text_player_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_player_counter.setForeground(new java.awt.Color(0, 51, 255));
        Text_player_counter.setText("Players:");

        Player_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Player_counter.setForeground(new java.awt.Color(0, 51, 255));
        Player_counter.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Player_counter.setText("NaN");

        Text_virus_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_virus_counter.setForeground(new java.awt.Color(0, 153, 0));
        Text_virus_counter.setText("Virus:");

        Virus_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Virus_counter.setForeground(new java.awt.Color(0, 153, 0));
        Virus_counter.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Virus_counter.setText("NaN");

        Text_aglomerados_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_aglomerados_counter.setForeground(new java.awt.Color(204, 0, 0));
        Text_aglomerados_counter.setText("Aglomerados:");

        Aglomerados_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Aglomerados_counter.setForeground(new java.awt.Color(204, 0, 0));
        Aglomerados_counter.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Aglomerados_counter.setText("NaN");

        Text_virus_counter1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Text_virus_counter1.setForeground(new java.awt.Color(153, 153, 153));
        Text_virus_counter1.setText("Info");

        Text_virus_counter2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Text_virus_counter2.setForeground(new java.awt.Color(153, 153, 153));
        Text_virus_counter2.setText("Agents");

        Button_Add_Agent.setBackground(new java.awt.Color(153, 0, 153));
        Button_Add_Agent.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Button_Add_Agent.setText("Add Agent");
        Button_Add_Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Button_Add_AgentActionPerformed(evt);
            }
        });

        Text_agent_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_agent_counter.setForeground(new java.awt.Color(153, 0, 153));
        Text_agent_counter.setText("Agents:");

        Agents_counter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Agents_counter.setForeground(new java.awt.Color(153, 0, 153));
        Agents_counter.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Agents_counter.setText("NaN");

        Text_virus_counter3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Text_virus_counter3.setForeground(new java.awt.Color(153, 153, 153));
        Text_virus_counter3.setText("Graphics");

        Text_agent_name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_agent_name.setForeground(new java.awt.Color(0, 0, 153));
        Text_agent_name.setText("Name:");

        Agent_name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Agent_name.setForeground(new java.awt.Color(0, 0, 153));
        Agent_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Agent_name.setText("NaN");

        Agent_type.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Agent_type.setForeground(new java.awt.Color(0, 0, 153));
        Agent_type.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Agent_type.setText("NaN");

        Text_agent_type.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_agent_type.setForeground(new java.awt.Color(0, 0, 153));
        Text_agent_type.setText("Type:");

        Text_agent_score.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_agent_score.setForeground(new java.awt.Color(0, 0, 153));
        Text_agent_score.setText("Score:");

        Agent_score.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Agent_score.setForeground(new java.awt.Color(0, 0, 153));
        Agent_score.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Agent_score.setText("NaN");

        Button_Show_Agent.setBackground(new java.awt.Color(153, 0, 153));
        Button_Show_Agent.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Button_Show_Agent.setText("Show");
        Button_Show_Agent.setEnabled(false);
        Button_Show_Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Button_Show_AgentActionPerformed(evt);
            }
        });

        Button_Remove_Agent.setBackground(new java.awt.Color(0, 51, 204));
        Button_Remove_Agent.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Button_Remove_Agent.setText("Remove");
        Button_Remove_Agent.setEnabled(false);
        Button_Remove_Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Button_Remove_AgentActionPerformed(evt);
            }
        });

        Check_Antialiasing.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Check_Antialiasing.setForeground(new java.awt.Color(0, 0, 153));
        Check_Antialiasing.setText("Anti-Aliasing");
        Check_Antialiasing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Check_AntialiasingActionPerformed(evt);
            }
        });

        Tick_rate_value.setMinimum(1);
        Tick_rate_value.setValue(30);
        Tick_rate_value.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                Tick_rate_valueStateChanged(evt);
            }
        });

        Text_tick_rate_slider.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_tick_rate_slider.setForeground(new java.awt.Color(0, 0, 153));
        Text_tick_rate_slider.setText("Tick-Rate:");

        Tick_rate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Tick_rate.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Tick_rate.setText("60");

        Text_refresh_rate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Text_refresh_rate.setText("Tickrate:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(game_frame, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Text_virus_counter3)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Text_agent_type, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Text_agent_score, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Text_agent_name, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(Agent_score, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                            .addComponent(Agent_type, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Agent_name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Text_tick_rate_slider)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Tick_rate_value, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Text_refresh_rate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Tick_rate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Text_frame_counter)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Frame_counter))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Text_aglomerados_counter)
                            .addComponent(Text_virus_counter, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Text_agent_counter, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Text_player_counter, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(Player_counter)
                            .addComponent(Aglomerados_counter, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(Virus_counter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Agents_counter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(Text_virus_counter1)
                    .addComponent(Text_virus_counter2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(Button_Exit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(Button_Add_Agent, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(9, 9, 9)
                            .addComponent(Button_Add_Player, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Check_Antialiasing, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jSeparator4)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(Button_Show_Agent, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(9, 9, 9)
                            .addComponent(Button_Remove_Agent, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Frame_counter)
                            .addComponent(Text_frame_counter)
                            .addComponent(Tick_rate)
                            .addComponent(Text_refresh_rate))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Text_virus_counter2)
                        .addGap(5, 5, 5)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Text_agent_name)
                            .addComponent(Agent_name))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Agent_type)
                            .addComponent(Text_agent_type))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Agent_score)
                            .addComponent(Text_agent_score))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Button_Remove_Agent, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Button_Show_Agent, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Text_virus_counter3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Tick_rate_value, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Text_tick_rate_slider))
                        .addGap(13, 13, 13)
                        .addComponent(Check_Antialiasing)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Text_virus_counter1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Text_aglomerados_counter)
                            .addComponent(Aglomerados_counter))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Virus_counter)
                            .addComponent(Text_virus_counter))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Agents_counter)
                            .addComponent(Text_agent_counter))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Player_counter)
                            .addComponent(Text_player_counter))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Button_Add_Player, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Button_Add_Agent, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Button_Exit, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(game_frame, javax.swing.GroupLayout.DEFAULT_SIZE, 654, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //----- METHODS -----//
    
    public int getMessages_rate() {
        return this.messages_rate;
    }
    
    public int getMessage_tick() {
        return this.message_tick;
    }
    
    public int getTick_rate() {
        return this.tick_rate;
    }
    
    public int getRefresh_rate() {
        return this.refresh_rate;
    }

    public Boolean getAntialiasing() {
        return this.antialiasing;
    }
    
    public Petri getTable() {
        return this.table;
    }

    public int getTableWidth() {
        return this.width;
    }

    public int getTableHeight() {
        return this.height;
    }
    
    public Platform_Controller getAgentsPlatform(){
        return this.agents_platform;
    }

    public JLabel getAgent_name() {
        return this.Agent_name;
    }

    public JLabel getAgent_score() {
        return this.Agent_score;
    }

    public JLabel getAgent_type() {
        return this.Agent_type;
    }

    public JLabel getFrame_counter() {
        return this.Frame_counter;
    }

    public JLabel getAgents_counter() {
        return this.Agents_counter;
    }

    public JLabel getAglomerados_counter() {
        return this.Aglomerados_counter;
    }

    public JLabel getPlayer_counter() {
        return this.Player_counter;
    }

    public JLabel getVirus_counter() {
        return this.Virus_counter;
    }

    public void setAgent_name(String name) {
        synchronized(this.Agent_name){
            this.Agent_name.setText(name);
        }
    }

    public void setAgent_score(String score) {
        synchronized(this.Agent_score){
            this.Agent_score.setText(score);
        }
    }

    public void setAgent_type(String type) {
        synchronized(this.Agent_type){
            this.Agent_type.setText(type);
        }
    }

    public void setFrame_counter(String frames) {
        this.Frame_counter.setText(frames);
    }

    public void setAgents_counter(String agents) {
        this.Agents_counter.setText(agents);
    }

    public void setAglomerados_counter(String aglomerados) {
        this.Aglomerados_counter.setText(aglomerados);
    }

    public void setPlayer_counter(String players) {
        this.Player_counter.setText(players);
    }

    public void setVirus_counter(String virus) {
        this.Virus_counter.setText(virus);
    }

    public void setRefresh_rate(JLabel Refresh_rate) {
        this.Tick_rate = Refresh_rate;
    }
    
    public void setShow(boolean s) {
        this.Button_Show_Agent.setEnabled(s);
    }
    
    public void setRemove(boolean r) {
        this.Button_Remove_Agent.setEnabled(r);
    }
    
    //----- OTHER METHODS -----//
    
    public void addPlayer(){
        Player p = this.table.addPlayer();
        this.agents_platform.startAgentInPlatform(this, p.clone(), "Agents.Basic_Player");
    }
    
    public void addAgent(){
        Player a = this.table.addAgent();
        this.agents_platform.startAgentInPlatform(this, a.clone(), "Agents.Basic_Agent");
    }
    
    public void openAgentWindow(String name){
        this.agents_platform.openAgentWindow(name);
    }
    
    private void Button_ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Button_ExitActionPerformed
        this.dispose();
        this.agents_platform.removeServicesFromPlatform("service_players");
        this.agents_platform.removeServicesFromPlatform("service_virus");
        this.agents_platform.removeServicesFromPlatform("service_aglomerados");
        System.exit(0);
    }//GEN-LAST:event_Button_ExitActionPerformed

    private void Button_Add_PlayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Button_Add_PlayerActionPerformed
        addPlayer();
    }//GEN-LAST:event_Button_Add_PlayerActionPerformed

    private void Button_Add_AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Button_Add_AgentActionPerformed
        addAgent();
    }//GEN-LAST:event_Button_Add_AgentActionPerformed

    private void Button_Show_AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Button_Show_AgentActionPerformed
        this.openAgentWindow(this.Agent_name.getText());
    }//GEN-LAST:event_Button_Show_AgentActionPerformed

    private void Button_Remove_AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Button_Remove_AgentActionPerformed
        String agent_name = this.Agent_name.getText();
        this.agents_platform.removeAgentFromPlatform(agent_name);
        
        this.table.removePlayer(agent_name);
        
        this.Button_Show_Agent.setEnabled(false);
        this.Button_Remove_Agent.setEnabled(false);
    }//GEN-LAST:event_Button_Remove_AgentActionPerformed

    private void Check_AntialiasingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Check_AntialiasingActionPerformed
        this.antialiasing = this.Check_Antialiasing.isSelected();
    }//GEN-LAST:event_Check_AntialiasingActionPerformed

    private void Tick_rate_valueStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_Tick_rate_valueStateChanged
        this.tick_rate = this.Tick_rate_value.getValue()*2;
        this.refresh_rate = 1000/this.tick_rate;
        this.Tick_rate.setText(""+this.tick_rate);
    }//GEN-LAST:event_Tick_rate_valueStateChanged
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GameMaster.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GameMaster.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GameMaster.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GameMaster.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new GameMaster().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Agent_name;
    private javax.swing.JLabel Agent_score;
    private javax.swing.JLabel Agent_type;
    private javax.swing.JLabel Agents_counter;
    private javax.swing.JLabel Aglomerados_counter;
    private javax.swing.JButton Button_Add_Agent;
    private javax.swing.JButton Button_Add_Player;
    private javax.swing.JButton Button_Exit;
    private javax.swing.JButton Button_Remove_Agent;
    private javax.swing.JButton Button_Show_Agent;
    private javax.swing.JCheckBox Check_Antialiasing;
    private javax.swing.JLabel Frame_counter;
    private javax.swing.JLabel Player_counter;
    private javax.swing.JLabel Text_agent_counter;
    private javax.swing.JLabel Text_agent_name;
    private javax.swing.JLabel Text_agent_score;
    private javax.swing.JLabel Text_agent_type;
    private javax.swing.JLabel Text_aglomerados_counter;
    private javax.swing.JLabel Text_frame_counter;
    private javax.swing.JLabel Text_player_counter;
    private javax.swing.JLabel Text_refresh_rate;
    private javax.swing.JLabel Text_tick_rate_slider;
    private javax.swing.JLabel Text_virus_counter;
    private javax.swing.JLabel Text_virus_counter1;
    private javax.swing.JLabel Text_virus_counter2;
    private javax.swing.JLabel Text_virus_counter3;
    private javax.swing.JLabel Tick_rate;
    private javax.swing.JSlider Tick_rate_value;
    private javax.swing.JLabel Virus_counter;
    private javax.swing.JInternalFrame game_frame;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    // End of variables declaration//GEN-END:variables
}
