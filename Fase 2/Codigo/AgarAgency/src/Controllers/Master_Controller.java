package Controllers;

import Data.Aglomerado;
import Data.LimitBox;
import Data.Player;
import Data.Virus;
import Presentation.GameMaster;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Master_Controller extends JPanel{
    
    // ----- VARIABLES ----- //

        // Father
        private GameMaster father;
        
        //Panel location
        private Master_Controller self;
        private Point origin;

        private Graphics2D graphics_main;
        private RenderingHints image_antialising;
        private LimitBox limit;

        // Graphics Thread
        private Thread paint_thread;
        private Thread frame_thread;
        private Thread maintenance_thread;

        // Frame counter
        private int frames;
        
        // Mouse position
        private int mouseX;
        private int mouseY;
        
        // Zoom control
        private double player_zoom;
        
        // Closest player
        private String closest_player;
    
    // ----- CONSTRUCTORS ----- //
        
    public Master_Controller(GameMaster gm){
        this.father = gm;
        this.self = this;
        this.origin = new Point(0, 0);
        
        this.limit = new LimitBox(Color.BLACK, this.father.getTableWidth(), this.father.getTableHeight());
        
        this.paint_thread = new Thread(new Master_Controller.paintAll());
        this.frame_thread = new Thread(new Master_Controller.frameCounter());
        this.maintenance_thread = new Thread(new Master_Controller.maintenancePlayers());
                
        this.frames = 0;
        
        this.player_zoom = 1.0;
        init();
    }
    
    // ----- METHODS ----- //
    
    private void init(){
        this.setBackground(Color.WHITE);

        this.setFocusable(true);
        this.requestFocusInWindow();
        
        this.image_antialising = new RenderingHints(
        RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
        
        this.addMouseWheelListener(new Master_Controller.MyMouseWheelListener());
        this.addMouseMotionListener(new Master_Controller.MyMouseMotionListener());
        this.addMouseListener(new Master_Controller.MyMouseListener());
        
        this.paint_thread.start();
        this.frame_thread.start();
        this.maintenance_thread.start();
    }
    
    @Override
    public void paint(Graphics gr){
        super.paintComponent(gr);
        
        this.graphics_main = (Graphics2D) gr.create();
        AffineTransform at = this.graphics_main.getTransform();
        
        // ANTIALISING?
        if(this.father.getAntialiasing()) this.graphics_main.setRenderingHints(this.image_antialising);
        
        // CENTER ON SELECTED PLAYER
        if(this.father.getTable().getPlayer(this.closest_player) != null){
            at.translate(this.getWidth() * 0.5 - this.father.getTable().getPlayer(this.closest_player).getPosX() * this.player_zoom, this.getHeight() * 0.5 - this.father.getTable().getPlayer(this.closest_player).getPosY() * this.player_zoom);
            at.scale(this.player_zoom, this.player_zoom);
            this.graphics_main.transform(at);
        }
        else{
            at.translate(this.origin.x, this.origin.y);
            this.graphics_main.transform(at);
        }
        
        // PAINT LIMITS
        this.limit.paint(this.graphics_main);
        
        // PAINT AGLOMERADOS
        synchronized(this.father.getTable().getAglomeradosPointer()){
            Iterator<String> it_aglomerados = this.father.getTable().getAglomeradosPointer().keySet().iterator();
            while(it_aglomerados.hasNext()){
                String key = it_aglomerados.next();
                Aglomerado a = this.father.getTable().getAglomeradosPointer().get(key);
                a.paint_aglomerado(this.graphics_main);
            }
        }
        
        // PAINT VIRUS
        synchronized(this.father.getTable().getVirusPointer()){
            Iterator<String> it_virus = this.father.getTable().getVirusPointer().keySet().iterator();
            while(it_virus.hasNext()){
                String key = it_virus.next();
                Virus v = this.father.getTable().getVirusPointer().get(key);
                v.paint_virus(this.graphics_main);
            }
        }
        
        // PAINT PLAYERS
        synchronized(this.father.getTable().getPlayersPointer()){
            Iterator<String> it_players = this.father.getTable().getPlayersPointer().keySet().iterator();
            while(it_players.hasNext()){
                String name = it_players.next();
                Player p = this.father.getTable().getPlayersPointer().get(name);
                p.paint_player(this.graphics_main, p.getDirection(), p.getRange());
            }
        }
        
        // DISPOSE GRAPHICS
        this.graphics_main.dispose();
        this.frames++;
    }
    
    // ----- RUNNABLES ----- //
    
    class paintAll implements Runnable {
        @Override
        public void run() {
            while(true){
                revalidate();
                repaint();
                try{
                    Thread.sleep(father.getRefresh_rate());
                }catch(InterruptedException e){
                    System.out.println("[CONTROLLER] Refresh - > Thread.sleep error!");
                }
            }
        }
    }
    
    class frameCounter implements Runnable {
        @Override
        public void run() {
            while(true){
                father.setFrame_counter(""+frames);
                frames = 0;
                
                // SET CLOSEST PLAYER SCORE INFO
                if(father.getTable().getPlayer(closest_player) != null){
                    father.setAgent_type(father.getTable().getPlayer(closest_player).getTypeName());
                    father.setAgent_score(""+father.getTable().getPlayer(closest_player).getScore());
                }
                else{
                    father.setAgent_name("NaN");
                    father.setAgent_type("NaN");
                    father.setAgent_score("NaN");
                    father.setShow(false);
                    father.setRemove(false);
                }
                
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    System.out.println("[CONTROLLER] Refresh - > Thread.sleep error!");
                }
            }
        }
    }
    
    class maintenancePlayers implements Runnable {
        @Override
        public void run() {
            while(true){
                // MAINTAIN NUMBER OF AGLOMERADOS
                int i;
                int aglomerados_count = father.getTable().getAglomeradosCurrent() - father.getTable().getAglomeradosNumber();
                int virus_count = father.getTable().getVirusCurrent() - father.getTable().getVirusNumber();

                if(aglomerados_count < 0){
                    for(i = 0; i < -aglomerados_count; i++){
                        father.getTable().removeAglomerado();
                    }
                }
                else{
                    for(i = 0; i < aglomerados_count; i++){
                        father.getTable().addAglomerado();
                    }
                }

                // MAINTAIN NUMBER OF VIRUS
                if(virus_count < 0){
                    for(i = 0; i < -virus_count; i++){
                        father.getTable().removeVirus();
                    }
                    repaint();
                }
                else{
                    for(i = 0; i < virus_count; i++){
                        father.getTable().addVirus();
                    }
                    repaint();
                }
                
                // MAINTENANCE REPORT
                father.setAglomerados_counter(""+father.getTable().getAglomeradosNumber());
                father.setVirus_counter(""+father.getTable().getVirusNumber());
                father.setAgents_counter(""+father.getTable().getAgentsNumber());
                father.setPlayer_counter(""+father.getTable().getPlayersNumber());
                
                // WAIT BEFORE NEXT MAINTENANCE
                try{
                    Thread.sleep(500);
                }catch(InterruptedException e){
                    System.out.println("[GAMEMASTER-MAINTENCANE] Thread.sleep error!");
                }
            }
        }
    }
    
    // ----- MOUSE ADAPTER ----- //
    
    class MyMouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e){
            mouseX = e.getX();
            mouseY = e.getY();
            if(SwingUtilities.isLeftMouseButton(e)){
                synchronized(father.getTable().getPlayersPointer()){
                    boolean found = false;
                    Iterator<String> it_players = father.getTable().getPlayersPointer().keySet().iterator();
                    while(it_players.hasNext()){
                        String name = it_players.next();
                        Player p = father.getTable().getPlayersPointer().get(name);
                        if(!p.getName().contains("Player") && p.sights(mouseX - origin.x, mouseY - origin.y, p.getRange())){
                            found = true;
                            closest_player = p.getName();
                            father.setAgent_name(p.getName());
                            father.setAgent_type(p.getTypeName());
                            father.setShow(true);
                            father.setRemove(true);
                            break;
                        }
                    }
                    
                    if(found){
                        player_zoom = 1.0;
                    }
                    else{
                        closest_player = "";
                    }
                }
            }
            else{
                if(SwingUtilities.isRightMouseButton(e)){

                }
                else{
                    if(SwingUtilities.isMiddleMouseButton(e)){
                        
                    }
                }
            }
        }
    }
    
    class MyMouseMotionListener extends MouseAdapter {
        @Override
        public void mouseMoved(MouseEvent e) {
            
        }
        
        @Override
        public void mouseDragged(MouseEvent e) {
            origin.setLocation(origin.getX() + e.getX() - mouseX, origin.getY() + e.getY() - mouseY);
            mouseX = e.getX();
            mouseY = e.getY();
        }
    }
    
    class MyMouseWheelListener extends MouseAdapter {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e){
            double player_zoom_factor = -0.05*e.getPreciseWheelRotation()*player_zoom;
            if(player_zoom + player_zoom_factor >= 0.2 && player_zoom + player_zoom_factor <= 15.0){
                player_zoom += player_zoom_factor;
            }
        }
    }
    
    // ----- OTHER METHODS ---- //
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(father.getTableWidth(), father.getTableHeight());
    };
    
}
