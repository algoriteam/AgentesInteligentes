package Controllers;

import Data.Aglomerado;
import Data.Crono;
import Data.LeaderBoard;
import Data.LimitBox;
import Data.Player;
import Data.TextBox;
import Data.Virus;
import Presentation.GameMaster;
import Presentation.PlayerScreen;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class Player_Controller extends JPanel{
    
    // ----- VARIABLES ----- //

        // GameMaster
        private GameMaster gm;
        
        // Father
        private PlayerScreen father;
        
        // Frame control
        private Double zoom = 1.0;
        private Graphics2D graphics_main;
        private RenderingHints image_antialising;

        // Graphics Thread
        private Thread paint_thread;
        private Thread frame_thread;
        private Thread jump_thread;

        // Text boxes
        private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        private LeaderBoard leaderboard;
        private LimitBox limit;
        private TextBox score;
        private TextBox impulse;
        private TextBox auto;
        private TextBox exit;
        private TextBox frame;
        private int frames;

        // Player
        private Player me;
        private GuiAgent agent;
        private float mouseX;
        private float mouseY;
        private Boolean auto_mode;
        
        private Double translated_x;
        private Double translated_y;
        
        // Calculate dislocation from elapsed time
        private Crono time;
        private float dislocation;

        // Jump
        private int elapsed_time;
    
    // ----- CONSTRUCTORS ----- //
        
    public Player_Controller(GameMaster m, PlayerScreen f, GuiAgent ag, Player pl){
        this.gm = m;
        this.father = f;
        this.me = pl;
        this.agent = ag;
        this.auto_mode = true;
        
        this.paint_thread = new Thread(new Player_Controller.paintAll());
        this.frame_thread = new Thread(new Player_Controller.frameCounter());
        this.jump_thread = new Thread(new Player_Controller.impulsePlayer());
        this.frames = 0;
        this.time = new Crono();
        
        Color transparent_gray = new Color(121, 126, 127, 125);
        Color transparent_blue = new Color(47, 98, 191, 125);
        Color transparent_red = new Color(191, 47, 47, 125);
        this.limit = new LimitBox(Color.BLACK, this.gm.getTableWidth(), this.gm.getTableHeight());
        this.frame = new TextBox("FPS: 0", Color.BLACK, transparent_gray, Color.WHITE, 20, 0, 0);
        this.score = new TextBox("Score: 30", Color.BLACK, transparent_blue, Color.WHITE, 20, 0, screenSize.height - 112);
        this.exit = new TextBox("RIGHT MOUSE - Exit!", Color.BLACK, transparent_red, Color.WHITE, 20, this.screenSize.width - 295, this.screenSize.height - 212);
        this.auto = new TextBox("MIDDLE MOUSE - Turn auto ON or OFF", Color.BLACK, transparent_blue, Color.WHITE, 20, this.screenSize.width - 460, this.screenSize.height - 162);
        this.impulse = new TextBox("LEFT MOUSE - Make an impulse", Color.BLACK, transparent_blue, Color.WHITE, 20, this.screenSize.width - 390, this.screenSize.height - 112);
        this.leaderboard = new LeaderBoard(this.me.getLeadPlayersPointer(), Color.BLACK, transparent_gray, Color.WHITE, 20, this.screenSize.width - 1, 0);
        
        this.elapsed_time = 0;
        init();
    }
    
    // ----- METHODS ----- //
    
    private void init(){
        this.setBackground(Color.WHITE);
        this.setPreferredSize(new Dimension(this.gm.getTableWidth(), this.gm.getTableHeight()));

        setFocusable(true);
        requestFocusInWindow();
        
        this.image_antialising = new RenderingHints(
        RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

        this.addMouseWheelListener(new Player_Controller.MyMouseWheelListener());
        this.addMouseMotionListener(new Player_Controller.MyMouseMotionListener());
        this.addMouseListener(new Player_Controller.MyMouseListener());
        
        this.paint_thread.start();
        this.frame_thread.start();
    }
    
    @Override
    public void paint(Graphics gr){
        super.paintComponent(gr);
        
        this.graphics_main = (Graphics2D) gr.create();
        AffineTransform at = this.graphics_main.getTransform();
        AffineTransform at_after = this.graphics_main.getTransform();
        
        // ANTIALISING?
        if(this.gm.getAntialiasing()) this.graphics_main.setRenderingHints(this.image_antialising);
        
        // CONTROL PLAYER POSITION
        this.me.updateVelocity();
        this.dislocation = this.me.getVelocity()*this.time.stop_milli();

            // Calculate new point
            if(this.auto_mode){
                this.me.updateAgentPosition(this.dislocation);
                this.me.validateAgentPos(this.dislocation, this.gm.getTableWidth(), this.gm.getTableHeight());
            }
            else{
                this.me.updatePlayerPosition(this.dislocation, this.mouseX, this.mouseY);
                this.me.validatePlayerPos(this.dislocation, this.gm.getTableWidth(), this.gm.getTableHeight());
            }
        
        // CENTER ON PLAYER
        this.translated_x = this.getWidth() * 0.5 - this.me.getPosX();
        this.translated_y = this.getHeight() * 0.5 - this.me.getPosY();
        at.translate(this.getWidth() * 0.5 - this.me.getPosX() * this.zoom, this.getHeight() * 0.5 - this.me.getPosY() * this.zoom);
        at.scale(this.zoom, this.zoom);
        this.graphics_main.transform(at);
        
        // PAINT LIMITS
        this.limit.paint(this.graphics_main);
        
        // PAINT AGLOMERADOS
        synchronized(this.me.getNearAglomeradosPointer()){
            Iterator<Aglomerado> it_aglomerados = this.me.getNearAglomeradosPointer().getNear().iterator();
            while(it_aglomerados.hasNext()){
                Aglomerado a = it_aglomerados.next();
                if(this.me.killed(a)){
                    // SEND GUI EVENT TO REMOVE AGLOMERADO
                    GuiEvent ge = new GuiEvent(a.getName(),3);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.incrementScore(a.getScore());
                }
                a.paint_aglomerado(this.graphics_main);
            }
        }
        
        // PAINT VIRUS
        synchronized(this.me.getNearVirusPointer()){
            Iterator<Virus> it_virus = this.me.getNearVirusPointer().getNear().iterator();
            while(it_virus.hasNext()){
                Virus v = it_virus.next();
                if(this.me.getScore() > 30 && this.me.killed(v) && v.getSize() < this.me.getSize()){
                    // SEND GUI EVENT TO REMOVE VIRUS
                    GuiEvent ge = new GuiEvent(v.getName(),2);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.breakInVirus();
                }
                v.paint_virus(this.graphics_main);
            }
        }

        // PAINT PLAYERS
        synchronized(this.me.getNearPlayersPointer()){
            Iterator<Player> it_players = this.me.getNearPlayersPointer().getNear().iterator();
            while(it_players.hasNext()){
                Player p = it_players.next();
                if(this.me.getScore() > 30 && this.me.killed(p) && p.getSize() < this.me.getSize() && !p.getName().equals(this.me.getName())){
                    GuiEvent ge = new GuiEvent(p.getName(),1);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.incrementScore(p.getScore());
                }
                p.paint_player(this.graphics_main, p.getDirection(), p.getRange());
            }
        }
        
        // PAINT PLAYER
        this.me.paint_player(this.graphics_main, this.me.getDirection(), this.me.getRange());
        this.score.setText("Score: " + Math.round(this.me.getScore()));
        
        this.graphics_main.setTransform(at_after);
        
        // PAINT BOXES
        this.frame.paint(this.graphics_main);
        this.score.paint(this.graphics_main);
        this.impulse.paint(this.graphics_main);
        this.auto.paint(this.graphics_main);
        this.exit.paint(this.graphics_main);
        this.leaderboard.paint(this.graphics_main);
        
        // DISPOSE GRAPHICS
        this.graphics_main.dispose();
        this.frames++;
        
        this.time.start();
    }
    
    // ----- RUNNABLES ---- //
    
    class paintAll implements Runnable {
        @Override
        public void run() {
            while(true){
                revalidate();
                repaint();
                
                // CHECK IF AGENT IS JUMPING
                if(auto_mode && me.isTryingToJump() && !jump_thread.isAlive()){
                    jump_thread = new Thread(new Player_Controller.impulseAgent());
                    jump_thread.start();
                }
                
                try{
                    Thread.sleep(gm.getRefresh_rate());
                }catch(InterruptedException e){
                    System.out.println("[PLAYERS] Paint - > Thread.sleep error!");
                }
            }
        }
    }
    
    class frameCounter implements Runnable {
        @Override
        public void run() {
            while(true){
                frame.setText("FPS: "+frames);
                frames = 0;
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    System.out.println("[PLAYERS] Frame counter - > Thread.sleep error!");
                }
            }
        }
    }
    
    class impulsePlayer implements Runnable {
        @Override
        public void run() {
            me.isJumping();
            me.breakInJump();
            score.setText("Score: " + Math.round(me.getScore()));
            while(elapsed_time < gm.getTick_rate() * me.getJumpTime()){
                elapsed_time++;
                me.setVelocityFactor(me.getJumpVelocity());
                
                try{
                    Thread.sleep(gm.getRefresh_rate());
                }catch(InterruptedException e){
                    System.out.println("[PLAYERS] Impulse - > Thread.sleep error!");
                }
            }
            elapsed_time = 0;
            me.notJumping();
        }
    }
    
    class impulseAgent implements Runnable {
        @Override
        public void run() {
            me.breakInJump();
            while(elapsed_time < gm.getMessages_rate() * me.getJumpTime()){
                elapsed_time++;
                me.setVelocityFactor(me.getJumpVelocity());
                
                try{
                    Thread.sleep(gm.getMessage_tick());
                }catch(InterruptedException e){
                    System.out.println("[AGENTS] Impulse - > Thread.sleep error!");
                }
            }
            elapsed_time = 0;
            me.notJumping();
        }
    }
    
    // ----- MOUSE ADAPTER ---- //
    
    class MyMouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e){
            if(SwingUtilities.isLeftMouseButton(e)){
                if(!auto_mode && me.getScore() - me.getScore() * me.getJumpLosingFactor() >= 30.0f && !jump_thread.isAlive()){
                    jump_thread = new Thread(new Player_Controller.impulsePlayer());
                    jump_thread.start();
                }
            }
            else{
                if(SwingUtilities.isRightMouseButton(e)){
                    // SEND GUI EVENT TO REMOVE PLAYER
                    GuiEvent ge = new GuiEvent(me.getName(),1);
                    agent.postGuiEvent(ge);
                }
                else{
                    if(SwingUtilities.isMiddleMouseButton(e)){
                        auto_mode = !auto_mode;
                    }
                }
            }
        }
    }
    
    class MyMouseMotionListener extends MouseAdapter {
        @Override
        public void mouseMoved(MouseEvent e) {
            e.translatePoint(-translated_x.intValue(), -translated_y.intValue());
            mouseX = e.getX();
            mouseY = e.getY();
        }
    }
    
    class MyMouseWheelListener extends MouseAdapter {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e){
            double zoomFactor = -0.05 * e.getPreciseWheelRotation() * zoom;
            if(zoom + zoomFactor >= 0.2 && zoom + zoomFactor <= 15.0){
                zoom += zoomFactor;
            }
        }
    }
    
    // ----- OTHER METHODS ---- //
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(this.gm.getTableWidth(), this.gm.getTableHeight());
    };
    
}
