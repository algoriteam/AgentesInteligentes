package Controllers;

import Data.Player;
import Presentation.AgentScreen;
import Presentation.GameMaster;
import jade.core.Runtime;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import java.util.HashMap;

public class Platform_Controller {
    
    // ----- VARIABLES ----- //
            
            // JADE console
            private Runtime rt;
            private ContainerController container;
            
            // JADE Agents pointers, usefull to remove them or manipulate them
            private HashMap<String, AgentController> agents;
            private HashMap<String, AgentController> services;
            
            // JADE Agents play screens
            private HashMap<String, AgentScreen> agent_screens;
    
    // ----- METHODS ----- //
    
    public void initMainContainer(String host, String port) {
        this.rt = Runtime.instance();
        
        Profile prof = new ProfileImpl();
        prof.setParameter(Profile.PLATFORM_ID, "AgarAgency Server");
        prof.setParameter(Profile.MAIN_HOST, host);
        prof.setParameter(Profile.MAIN_PORT, port);
        prof.setParameter(Profile.MAIN, "true");
        prof.setParameter(Profile.GUI, "true");
        
        this.container = rt.createMainContainer(prof);
        this.rt.setCloseVM(true);
        
        this.agents = new HashMap<>();
        this.services = new HashMap<>();
        this.agent_screens = new HashMap<>();
    }

    
    //----- AGENTS -----//
    
    public void startAgentInPlatform(GameMaster m, Player c, String classpath){
        synchronized(this.agents){
            try {
                AgentController ac = this.container.createNewAgent(c.getName(), classpath, new Object[] {m, c});
                this.agents.put(c.getName(), ac);

                ac.start();
            } 
            catch (StaleProxyException e) {
                System.out.println("[PLATFORM-CONTROLLER] Error creating Agent '"+ c.getName() + "'");
            }
        }
    }
    
    public void removeAgentFromPlatform(String name){
        synchronized(this.agents){
            AgentController ac = this.agents.get(name);
            if(ac != null){
                try{
                    ac.kill();
                    this.agents.remove(name);
                }
                catch(StaleProxyException e) {
                    System.out.println("[PLATFORM-CONTROLLER] Error removing Agent '"+ name + "'");
                }
            }
            if(name.contains("Agent")) this.removeAgentWindow(name);
        }
    }
    
    //----- PLAY SCREENS -----//
    
    public void addAgentWindow(String name, AgentScreen as){
        synchronized(this.agent_screens){
            this.agent_screens.put(name, as);
        }
    }
    
    public void removeAgentWindow(String name){
        synchronized(this.agent_screens){
            this.agent_screens.get(name).dispose();
            this.agent_screens.remove(name);
        }
    }
    
    public void openAgentWindow(String name){
        synchronized(this.agent_screens){
            AgentScreen as = this.agent_screens.get(name);
            if(as != null){
                as.setVisible(true);
            }
        }
    }
    
    public void closeAgentWindow(String name){
        synchronized(this.agent_screens){
            AgentScreen as = this.agent_screens.get(name);
            if(as != null){
                as.setVisible(false);
            }
        }
    }
    
    //----- SERVICES -----//
    
    public void startServiceInPlatform(String name, GameMaster gm, String classpath){
        synchronized(this.services){
            try {
                AgentController ac = this.container.createNewAgent(name, classpath, new Object[] {gm});
                this.agents.put(name,ac);

                ac.start();
            } 
            catch (StaleProxyException e) {
                System.out.println("[PLATFORM-CONTROLLER] Error starting Service '"+ name + "'");
            }
        }
    }
    
    public void removeServicesFromPlatform(String name){
        synchronized(this.services){
            AgentController ac = this.services.get(name);
            if(ac != null){
                try{
                    ac.kill();
                    this.services.remove(name);
                }
                catch(StaleProxyException e) {
                    System.out.println("[PLATFORM-CONTROLLER] Error ending Service '"+ name + "'");
                }
            }
        }
    }
}
