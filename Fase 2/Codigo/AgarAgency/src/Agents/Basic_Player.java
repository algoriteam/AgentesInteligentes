package Agents;

import Behaviours.PlayerReceiveAllPackages;
import Behaviours.SendPlayerStatus;
import Data.Player;
import Presentation.GameMaster;
import Presentation.PlayerScreen;
import jade.core.AID;
import jade.core.behaviours.ParallelBehaviour;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import java.io.IOException;


public class Basic_Player extends GuiAgent{
    
    //----- VARIABLES -----//
    
        // GameMaster
        private GameMaster gm;
        
        // Player
        private PlayerScreen player_screen;
        private Player me;
        
    //----- METHODS -----//

    @Override
    protected void setup(){
        super.setup();
        System.out.println("[AGENTS] Player '"+ this.getLocalName() + "' joined the party");
        
        Object[] args = getArguments();
        if(args != null && args.length > 0){
            this.gm = (GameMaster) args[0];
            this.me = (Player) args[1];
            
            this.player_screen = new PlayerScreen(this.gm, this, this.me);
            this.player_screen.setVisible(true);
            
            ParallelBehaviour pb = new ParallelBehaviour(this, ParallelBehaviour.WHEN_ALL){
                
                @Override
                public int onEnd(){
                    myAgent.doDelete();
                    return 0;
                }
                
            };
            
            pb.addSubBehaviour(new PlayerReceiveAllPackages(this.gm, this.player_screen, this.me));
            pb.addSubBehaviour(new SendPlayerStatus(this, this.gm.getMessage_tick(), this.me));
            this.addBehaviour(pb);
        }
    }

    
    @Override
    protected void takeDown(){
        super.takeDown();
        System.out.println("[AGENTS] Player '" + this.getLocalName() + "' left the party");
    }

    @Override
    protected void onGuiEvent(GuiEvent ge) {  
        int command = ge.getType();
        String remove_name = (String) ge.getSource();
        AID receiver = new AID();
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        
        switch(command){
            // REMOVE PLAYER
            case 1:
                try{
                    // Set receiver
                    receiver.setLocalName("service_players");

                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("32");
                    msg.setContentObject(remove_name);

                    // Send message
                    this.send(msg);
                }
                catch(IOException e){
                    System.out.println("[AGENTS] Player '" + this.getLocalName() + "' couldn't send player name to remove");
                }
                break;
            // REMOVE VIRUS
            case 2:
                try{
                    // Set receiver
                    receiver.setLocalName("service_virus");

                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("22");
                    msg.setContentObject(remove_name);

                    // Send message
                    this.send(msg);
                }
                catch(IOException e){
                    System.out.println("[AGENTS] Player '" + this.getLocalName() + "' couldn't send virus name to remove");
                }
                break;
            // REMOVE AGLOMERADO
            case 3:
                try{
                    // Set receiver
                    receiver.setLocalName("service_aglomerados");

                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("12");
                    msg.setContentObject(remove_name);

                    // Send message
                    this.send(msg);
                }
                catch(IOException e){
                    System.out.println("[AGENTS] Player '" + this.getLocalName() + "' couldn't send aglomerado name to remove");
                }
                break;
            default:
                System.out.println("[AGENTS] Invalid command stated on GuiEvent - (ID = " + command + ")");
                break;
        }
    }
}
