package Agents_Example;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;


public class GMAgentList extends Agent{
    @Override
    protected void setup(){
        super.setup();
        
        DFAgentDescription df = new DFAgentDescription();
        df.setName(this.getAID());
        
        ServiceDescription sd = new ServiceDescription();
        sd.setType("petri");
        sd.setName("JADE-replies-petri");
        df.addServices(sd);
        
        try{
            DFService.register(this, df);
            System.out.println("[OK]" + this.getLocalName() + " starting!");
        
            //this.addBehaviour(/*new bahaviour name*/);
        }
        catch(FIPAException e){
            System.out.println("[ERROR] Not able to register service");
        }
    }
    
    @Override
    protected void takeDown(){
        super.takeDown();
        
        try{
            DFService.deregister(this);
            System.out.println("[OK]" + this.getLocalName() + " died with success!");
        }
        catch(FIPAException e){
            System.out.println("[ERROR] Not able to un-register service");
        }
    }
    
}
