package Packages;

import Data.Aglomerado;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.Iterator;

public class NearAglomerados implements Serializable{
    
    //----- VARIABLES -----//
    
            private ArrayList<Aglomerado> near_aglomerados;
    
    //----- CONSTRUCTORS -----//
    
    public NearAglomerados() {
        this.near_aglomerados = new ArrayList<>();
    }
    
    public NearAglomerados(NearAglomerados a) {
        this.near_aglomerados = new ArrayList<>();
        Iterator<Aglomerado> it_aglomerados = a.getNear().iterator();
        while(it_aglomerados.hasNext()){
            Aglomerado g = it_aglomerados.next();
            this.near_aglomerados.add(g.clone());
        }
    }
    
    //----- METHODS -----//

    public ArrayList<Aglomerado> getNear() {
        return this.near_aglomerados;
    }
    
    //----- OTHER METHODS -----//
    
    public void addNear(Aglomerado a){
        this.near_aglomerados.add(a);
    }
    
    @Override
    public NearAglomerados clone(){
        return new NearAglomerados(this);
    }

}
