package Packages;

import Data.Player;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.Iterator;

public class LeadPlayers implements Serializable{
    
    //----- VARIABLES -----//
    
            private ArrayList<Player> lead_players;
    
    //----- CONSTRUCTORS -----//
    
    public LeadPlayers() {
        this.lead_players = new ArrayList<>();
    }
    
    public LeadPlayers(LeadPlayers p) {
        this.lead_players = new ArrayList<>();
        Iterator<Player> it_players = p.getLead().iterator();
        while(it_players.hasNext()){
            Player c = it_players.next();
            this.lead_players.add(c.clone());
        }
    }
    
    //----- METHODS -----//

    public ArrayList<Player> getLead() {
        return this.lead_players;
    }
    
    public void setLeaders(LeadPlayers l){
        this.lead_players.clear();
        Iterator<Player> it_players = l.getLead().iterator();
        while(it_players.hasNext()){
            Player c = it_players.next();
            this.lead_players.add(c);
        }
    }
    
    //----- OTHER METHODS -----//
    
    public void addLead(Player p){
        this.lead_players.add(p);
    }
    
    @Override
    public LeadPlayers clone(){
        return new LeadPlayers(this);
    }

}
