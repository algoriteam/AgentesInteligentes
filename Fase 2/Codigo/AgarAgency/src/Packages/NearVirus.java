package Packages;

import Data.Player;
import Data.Virus;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.Iterator;

public class NearVirus implements Serializable{
    
    //----- VARIABLES -----//
    
            private ArrayList<Virus> near_virus;
    
    //----- CONSTRUCTORS -----//
    
    public NearVirus() {
        this.near_virus = new ArrayList<>();
    }
    
    //----- METHODS -----//

    public ArrayList<Virus> getNear() {
        return this.near_virus;
    }
    
    //----- OTHER METHODS -----//
    
    public void addNear(Virus v){
        this.near_virus.add(v);
    }
    
    public boolean hasPlayer(Player p){
        Iterator<Virus> it_virus = this.near_virus.iterator();
        while(it_virus.hasNext()){
            Virus v = it_virus.next();
            if(v.killed(p)) return true;
        }
        return false;
    }

}
