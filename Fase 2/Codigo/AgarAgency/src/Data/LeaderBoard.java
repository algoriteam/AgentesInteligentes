package Data;

import Packages.LeadPlayers;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Iterator;

public class LeaderBoard {
    
    //----- VARIABLES -----//
            
            // Players
            private LeadPlayers players;
            
            // Colors and fonts
            private Color color_line;
            private Color color_background;
            private Color color_text;
            private int font_size;
            
            // Position
            private int offX;
            private int offY;
        
    //----- CONSTRUCTORS -----//
    
    public LeaderBoard(LeadPlayers p, Color cl, Color cb, Color ct, int fs, int x, int y){
        this.players = p;
        
        this.color_line = cl;
        this.color_background = cb;
        this.color_text = ct;
        this.font_size = fs;
        
        this.offX = x;
        this.offY = y;
    }
    
    //----- METHODS -----//
    
    //----- GETS -----//

    public Color getColor_line() {
        return this.color_line;
    }

    public Color getColor_background() {
        return this.color_background;
    }

    public Color getColor_text() {
        return this.color_text;
    }

    public int getFont_size() {
        return this.font_size;
    }

    public int getOffX() {
        return this.offX;
    }

    public int getOffY() {
        return this.offY;
    }
    
    //----- SETS -----//

    public void setColor_line(Color cl) {
        this.color_line = cl;
    }

    public void setColor_background(Color cb) {
        this.color_background = cb;
    }

    public void setColor_text(Color ct) {
        this.color_text = ct;
    }

    public void setFont_size(int fs) {
        this.font_size = fs;
    }

    public void setOffX(int x) {
        this.offX = x;
    }

    public void setOffY(int y) {
        this.offY = y;
    }
    
    //----- OTHER METHODS -----//
    
    //----- PAINT -----//
    
    public void paint(Graphics2D g){
        synchronized(this.players){
            Font font = new Font("TimesRoman", Font.PLAIN, this.getFont_size());
            FontMetrics metrics = g.getFontMetrics(font);
            int big_string_width = 0;
            int size = this.players.getLead().size();

            // Search for biggest player name to set leaderboard draw width
            Iterator<Player> it_players1 = this.players.getLead().iterator();
            int i = 0;
            while(it_players1.hasNext()){
                Player p = it_players1.next();
                int string_width = metrics.stringWidth((i + 1) + ". " + p.getName() + " - " + p.getScore());
                if(string_width > big_string_width) big_string_width = string_width;
                i++;
            }

            // Draw background
            g.setColor(this.getColor_background());
            Rectangle.Float r1 = new Rectangle.Float(this.getOffX() - big_string_width - 100, this.getOffY(), big_string_width + 100, (size + 1) * 30);
            g.draw(r1);
            g.fill(r1);

            // Draw outerline
            g.setColor(this.getColor_line());
            Rectangle.Float r2 = new Rectangle.Float(this.getOffX() - big_string_width - 100, this.getOffY(), big_string_width + 100, (size + 1) * 30);
            g.draw(r2);

            // Draw title
            g.setFont(font);
            float y1 = ((30 - metrics.getHeight()) * 0.5f) + metrics.getAscent();
            g.drawString("LEADERBOARD", this.getOffX() - big_string_width - 45, this.getOffY() + y1);

            // Draw player names
            g.setColor(this.getColor_text());
            g.setFont(font);

            Iterator<Player> it_players2 = this.players.getLead().iterator();
            i = 0;
            while(it_players2.hasNext()){
                Player p = it_players2.next();
                float y2 = (i + 1 ) * 30 + ((30 - metrics.getHeight()) * 0.5f) + metrics.getAscent();
                g.drawString((i + 1) + ". " + p.getName() + " - " + p.getScore(), this.getOffX() - big_string_width - 45, this.getOffY() + y2);
                i++;
            }
        }
    }
    
}
