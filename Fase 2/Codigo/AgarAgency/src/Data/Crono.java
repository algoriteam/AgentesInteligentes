package Data;
public class Crono {
    //-----VARIABLES-----//
        private long start;
        private long end;
    //-----CONSTRUCTORS-----//
    public void Crono() {
        this.start = 0L;
        this.end = 0L;
    }
    
    //-----METHODS-----//
    // Starts counter
    public void start() {
        this.start = 0L;
        this.start = System.nanoTime();
    }
    // Stops counter and returns time in seconds
    public float stop() {
        this.end = System.nanoTime();
        return (this.end - this.start) * 0.000000001f;
    }
    
    // Resets counter
    public void reset() {
        this.start = 0L;
        this.end = 0L;
    }
    
    // Stops counter and returns time in milliseconds
    public float stop_milli() {
        this.end = System.nanoTime();
        return (this.end - this.start) * 0.001f;
    }
    
    // Stops counter and returns time in nanoseconds
    public float stop_nano() {
        this.end = System.nanoTime();
        return (this.end - this.start);
    }
    // Stops counter and returns time in seconds within a string
    public String print() {
        return "" + stop();
    }
}
