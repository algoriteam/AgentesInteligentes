package Data;

import java.io.Serializable;

public class Vetor implements Serializable{
    
    //----- VARIABLES -----//
            
            // Point
            private float dX;
            private float dY;
            
            // Distance
            private float distance;
    
    //----- CONSTRUCTORS -----//
        
    public Vetor() {
       this.dX = 0.0f;
       this.dY = 0.0f;
       this.distance = 0.0f;
    }

    public Vetor(float x, float y, float d) {
       this.dX = x;
       this.dY = y;
       this.distance = d;
    }

    public Vetor(float x, float y) {
       this.dX = x;
       this.dY = y;
       this.distance = (float) Math.sqrt(x * x + y * y);
    }

    public Vetor(Vetor v) {
       this.dX = v.getDX();
       this.dY = v.getDY();
       this.distance = v.getDistance();
    }
    
    //----- METHODS -----//
    
    //----- GETS -----//
    
    public float getDX() {
        return this.dX;
    }

    public float getDY() {
        return this.dY;
    }

    public float getDistance() {
        return this.distance;
    }
    
    //----- SETS -----//
    
    public void setDX(float x) {
        this.dX = x;
    }

    public void setDY(float y) {
        this.dY = y;
    }
    
    public void setDXDY(float x, float y) {
        this.dX = x;
        this.dY = y;
    }

    public void setDistance() {
        this.distance = (float) Math.sqrt(this.getDX() * this.getDX() + this.getDY() * this.getDY());
    }
    
    //----- OTHER METHODS -----//

    public void scale(float sf) {
        this.setDX(this.getDX() * sf);
        this.setDY(this.getDY() * sf);
        this.setDistance();
    }
    
    public float dotProduct(Vetor v) {
        return this.getDX() * v.getDX() + this.getDY() * v.getDY();
    }
    
    public void invert() {
        this.setDX(-this.getDX());
        this.setDX(-this.getDY());
    }

    public void normalize() {
        if (this.getDistance() > 0.0f) {
          this.setDX(this.getDX()/this.getDistance());
          this.setDY(this.getDY()/this.getDistance());
        }
    }
    
    //----- OVERRIDE METHODS -----//
    
    @Override
    public Vetor clone(){
        return new Vetor(this);
    }
    
    @Override
    public boolean equals(Object o){
        if(this==o) return true;
        if(o==null || this.getClass() != o.getClass()) return false;
        else{
            Vetor v = (Vetor) o;
            return this.getDX() == v.getDX() &&
                   this.getDY() == v.getDY() &&
                   this.getDistance() == v.getDistance();
        }
    }

}
