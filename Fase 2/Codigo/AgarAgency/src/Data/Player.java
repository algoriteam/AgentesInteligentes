package Data;

import Packages.LeadPlayers;
import Packages.NearAglomerados;
import Packages.NearPlayers;
import Packages.NearVirus;
import java.awt.Color;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

public class Player extends Dot implements Serializable{
    
    //----- VARIABLES -----//
    
        // Base
        private Vetor direction;
        private float velocity;
        
        // Type
        /*
            -1 - Player
            0 - Basic
            1 - Eagle Eye
            2 - Blind
            3 - Chicken
            4 - Panic
            5 - Stealth
        */ 
        private Integer type;
        
        // Controlled properties (Basic)
        
            // Velocity and range properties
            private float rangeFactor = 4; // Consoante o raio do jogador (Range = rangeFactor*raio onde raio = size/2)
            private float velocityFactor = 0.018f;

            // Jumping properties
            private float jumpVelocity = 0.0006f; // Velocidade do utilizador durante o salto (Velocidade normal = 0.0002 quando size = 30)
            private float jumpTime = 0.6f; // Tempo de salto em segundos
            private float jumpLosingFactor = 0.5f; // Valor que utilizador perde quando realiza um salto (Size -= Size*losingFactor)
            private Integer not_jumping = 1;
            
            // Virus properties
            private float virusLosingFactor = 0.5f; // Valor que utilizador perde quando toca num virus (Size -= Size*losingFactor)
            
        // Random angle
        private double angle;
        
        // Near elements
        private NearPlayers near_players;
        private NearVirus near_virus;
        private NearAglomerados near_aglomerados;
        
        // Leader Players
        private LeadPlayers lead_players;
        
        // Player Log
        private ArrayList<String> logger;
        
        // Status
        /*
            0 - Nothing
            1 - Eating
            2 - Running away
            3 - Going after someone
            4 - Getting away from virus
            5 - Hidding
            6 - Damn virus
            7 - Successfully ate someone
            8 - Impulse
            9 - Hiding spot
        */ 
        private int status;
        
        // PANIC PROPERTIES
        private float panicTime = 0.5f; // Tempo de panico em segundos
        private Integer not_panicking = 1;
        
        // STEALTH PROPERTIES
        private ArrayList<Virus> bushes;
        
    //----- CONSTRUCTORS -----//
    
    public Player() {
        super("", 0.0f, 0.0f, 0, 0, Color.WHITE);
        this.direction = new Vetor();
        this.velocity = 0.0f;
        
        this.type = 0;
        this.angle = 0.0f;
        
        this.near_players = new NearPlayers();
        this.near_virus = new NearVirus();
        this.near_aglomerados = new NearAglomerados();
        
        this.lead_players = new LeadPlayers();
        
        this.logger = new ArrayList<>();
        this.status = 0;
        
        this.bushes = new ArrayList<>();
    }
    
    public Player(String name, int x, int y, int ss, Color c, int t) {
        super(name, x, y, ss, ss, c);
        this.direction = new Vetor();
        this.velocity = 0.0f;
        
        this.type = t;
        this.angle = 0.0f;

        this.near_players = new NearPlayers();
        this.near_virus = new NearVirus();
        this.near_aglomerados = new NearAglomerados();
        
        this.lead_players = new LeadPlayers();
        
        this.logger = new ArrayList<>();
        this.status = 0;
        
        this.bushes = new ArrayList<>();
    }
    
    public Player(Player d) {
        super(d);
        this.direction = d.getDirection();
        this.velocity = d.getVelocity();
        this.type = d.getType();
        
        this.rangeFactor = d.getRangeFactor();
        this.velocityFactor = d.getVelocityFactor();
        
        this.jumpVelocity = d.getJumpVelocity();
        this.jumpTime = d.getJumpTime();
        this.jumpLosingFactor = d.getJumpLosingFactor();
        this.not_jumping = 1;
        
        this.virusLosingFactor = d.getVirusLosingFactor();
        
        this.angle = d.getAngle();
        
        this.near_players = new NearPlayers();
        this.near_virus = new NearVirus();
        this.near_aglomerados = new NearAglomerados();
        
        this.lead_players = new LeadPlayers();
        this.logger = new ArrayList<>();
        this.status = 0;
        
        this.bushes = new ArrayList<>();
    }
   
    //----- METHODS -----//
    
    //----- GETS -----//
    
    public Vetor getDirection() {
        return this.direction;
    }

    public float getVelocity() {
        return this.velocity;
    }
    
    public int getType(){
        synchronized(this.type){
            return this.type;
        }
    }
    
    public String getTypeName(){
        String type = "";
        switch(this.getType()){
            case -1:
                type = "Player";
                break;
            case 0:
                type = "Basic";
                break;
            case 1:
                type = "Eagle Eye";
                break;
            case 2:
                type = "Blind";
                break;
            case 3:
                type = "Chicken";
                break;
            case 4:
                type = "Panic";
                break;
            case 5:
                type = "Stealth";
                break;
        }
        return type;
    }
    
    public float getRangeFactor() {
        return this.rangeFactor;
    }
    
    public float getVelocityFactor() {
        return this.velocityFactor;
    }
    
    public float getJumpVelocity() {
        return this.jumpVelocity;
    }
    
    public float getJumpTime() {
        return this.jumpTime;
    }
    
    public float getJumpLosingFactor() {
        return this.jumpLosingFactor;
    }
    
    public boolean isTryingToJump(){
        synchronized(this.not_jumping){
            return this.not_jumping == 0;
        }
    }
    
    public void isJumping(){
        synchronized(this.not_jumping){
            this.not_jumping = 0;
        }
    }
    
    public void notJumping(){
        synchronized(this.not_jumping){
            this.not_jumping = 1;
        }
    }
    
    public float getVirusLosingFactor() {
        return this.virusLosingFactor;
    }
    
    public double getAngle(){
        return this.angle;
    }
    
    public NearPlayers getNearPlayersPointer(){
        return this.near_players;
    }
    
    public NearVirus getNearVirusPointer(){
        return this.near_virus;
    }
    
    public NearAglomerados getNearAglomeradosPointer(){
        return this.near_aglomerados;
    }
    
    public LeadPlayers getLeadPlayersPointer(){
        return this.lead_players;
    }
    
    public float getPanicTime() {
        return this.panicTime;
    }
    
    public boolean isTryingToPanic(){
        synchronized(this.not_panicking){
            return this.not_panicking == 0;
        }
    }
    
    public void isPanicking(){
        synchronized(this.not_panicking){
            this.not_panicking = 0;
        }
    }
    
    public void notPanicking(){
        synchronized(this.not_panicking){
            this.not_panicking = 1;
        }
    }
    
    public String getLog(){
        synchronized(this.logger){
            String result = "";
            Iterator<String> it_logs = this.logger.iterator();
            while(it_logs.hasNext()){
                String log = it_logs.next();
                result+=log + "\n";
            }
            return result;
        }
    }
    
    public int getStatus(){
        return this.status;
    }
    
    public String getStatusText() {
        String current_status = "";
        switch(this.getStatus()){
            case 0:
                current_status = "This looks like the desert";
                break;
            case 1:
                current_status = "Found some food!";
                break;
            case 2:
                current_status = "Someone is after me";
                break;
            case 3:
                current_status = "I found someone to eat";
                break;
            case 4:
                current_status = "Keeping a safe distance from a virus";
                break;
            case 5:
                current_status = "Im hiding in a nearby virus";
                break;
            case 6:
                current_status = "Damn, i got stuck in a virus";
                break;
            case 7:
                current_status = "I ate someone and it was delicious";
                break;
            case 8:
                current_status = "Uppp uppp hooorrrayy!";
                break;
            case 9:
               current_status = "I know a hiding spot :D";
               break;
        }
        return current_status;
    }
    
    public ArrayList<Virus> getBushes(){
        return this.bushes;
    }
    
    //----- SETS -----//

    public void setDirection(Vetor direction) {
        this.direction = direction;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }
    
    public void setType(int t){
        synchronized(this.type){
            switch(t){
                case 0:
                    this.addLog("I'm too basic now (BASIC)");
                    break;
                case 1:
                    this.addLog("I can see everything (EAGLE EYE)");
                    break;
                case 2:
                    this.addLog("Where is everybody? (BLIND)");
                    break;
                case 3:
                    this.addLog("Brbrbrbr, immmm a chicken now (CHICKEN)");
                    break;
                case 4:
                    this.addLog("Omg omg omg omg omg (PANIC)");
                    break;
                case 5:
                    this.addLog("Now you see me, now you don't (STEALTH)");
                    break;
            }
            this.type = t;
        }
    }
    
    public void setRangeFactor(float r){
        this.rangeFactor = r;
    }
    
    public void setVelocityFactor(float v) {
        this.velocityFactor = v;
    }
    
    public void setJumpVelocity(float j) {
        this.jumpVelocity = j;
    }
    
    public void setJumpTime(float t) {
        this.jumpTime= t;
    }
    
    public void setJumpLosingFactor(float l) {
        this.jumpLosingFactor = l;
    }
    
    public void setVirusLosingFactor(float v){
        this.virusLosingFactor = v;
    }
    
    public void setNearPlayers(NearPlayers p){
        this.near_players = p;
    }
    
    public void setNearVirus(NearVirus v){
        this.near_virus = v;
    }
    
    public void setNearAglomerados(NearAglomerados a){
        this.near_aglomerados = a;
    }
    
    public void setLeadPlayers(LeadPlayers l){
        this.lead_players.setLeaders(l);
    }

    public void setAngle(float a) {
        this.angle = a;
    }
    
    public void setStatus(int s){
        this.status = s;
    }
    
    public void setPanicTime(float t) {
        this.panicTime= t;
    }
    
    //----- OTHER METHODS -----//
    
    public boolean isAgent() {
        return this.type >= 0;
    }
    
    public float getRange(){
        float r = this.rangeFactor * this.getSize() * 0.5f;
        switch(this.getType()){
            case -1:
                r = r * 2;
                break;
            case 1:
                r = r * 2;
                break;
            case 2:
                r = r * 0.5f;
                break;
        }
        return r;
    }
    
    public void breakInVirus(){
        this.decrementScore(Math.round(this.getScore() * this.getVirusLosingFactor()));
        this.setStatus(6);
    }
    
    public void breakInJump(){
        this.decrementScore(Math.round(this.getScore() * this.getJumpLosingFactor()));
        this.setStatus(8);
    }
    
    public void updateVelocity() {
        this.velocity = (0.0125f / (this.getSize())) * not_jumping + velocityFactor * (1 - not_jumping);
    }
    
    // AGENT
    public void updateAgentPosition(float dislocation){  
        Vetor dir = this.getDirection();
        
        if(!this.isTryingToJump() && !this.isTryingToPanic()){
            float new_diffX;
            float new_diffY;

            double min_aglomerado_distance = 0.0;
            Aglomerado nearest_aglomerado = null;

            double min_small_player_distance = 0.0;
            double min_big_player_distance = 0.0;
            Player nearest_small_player = null;
            Player nearest_big_player = null;
            
            double min_small_virus_distance = 0.0;
            double min_big_virus_distance = 0.0;
            Virus nearest_small_virus = null;
            Virus nearest_big_virus = null;
            
            double min_bush_virus_distance = 0.0;
            Virus nearest_bush_virus = null;
        
            // Calculate nearest players
            synchronized(this.getNearPlayersPointer()){
                Iterator<Player> it_players = this.getNearPlayersPointer().getNear().iterator();
                while(it_players.hasNext()){
                    Player p = it_players.next();
                    double current_distance = this.euclidianDistance(p);
                    if(p.getSize() > this.getSize()){
                        if(current_distance < min_big_player_distance || min_big_player_distance == 0.0){
                            nearest_big_player = p;
                            min_big_player_distance = current_distance;
                        }
                    }
                    else {
                        if(p.getSize() < this.getSize()){
                            if(current_distance < min_small_player_distance || min_small_player_distance == 0.0){
                                nearest_small_player = p;
                                min_small_player_distance = current_distance;
                            }
                        }
                    }
                }
            }

            // Calculate nearest virus
            synchronized(this.getNearVirusPointer()){
                Iterator<Virus> it_virus = this.getNearVirusPointer().getNear().iterator();
                while(it_virus.hasNext()){
                    Virus v = it_virus.next();
                    double current_distance = this.euclidianDistance(v);
                    if(v.getSize() > this.getSize()){
                        this.addBush(v);
                        if(current_distance < min_big_virus_distance || min_big_virus_distance == 0.0){
                            nearest_big_virus = v;
                            min_big_virus_distance = current_distance;
                        }
                    }
                    else{
                        if(v.getSize() < this.getSize()){
                            this.removeBush(v);
                            if(current_distance < this.getSize()*0.6f){
                                nearest_small_virus = v;
                                min_small_virus_distance = current_distance;
                            }
                        }
                    }
                }
            }
            
            if(this.getType() == 5){
                // Calculate nearest bush
                synchronized(this.bushes){
                    Iterator<Virus> it_bushes = this.bushes.iterator();
                    while(it_bushes.hasNext()){
                        Virus v = it_bushes.next();
                        double current_distance = this.euclidianDistance(v);
                        if(v.getSize() > this.getSize()){
                            if(current_distance < min_bush_virus_distance || min_bush_virus_distance == 0.0){
                                nearest_bush_virus = v;
                                min_bush_virus_distance = current_distance;
                            }
                        }
                    }
                }
            }
            
            // Check if there's a bigger player
            if(nearest_big_player != null){
                if(nearest_big_virus != null){
                    // Hide in virus
                    new_diffX = nearest_big_virus.getPosX()-this.getPosX();
                    new_diffY = nearest_big_virus.getPosY()-this.getPosY();

                    this.setStatus(5);
                }
                else{
                    if(this.getType() == 5 && nearest_bush_virus != null){
                        // Go directly to the bush
                        new_diffX = nearest_bush_virus.getPosX() - this.getPosX();
                        new_diffY = nearest_bush_virus.getPosY() - this.getPosY();
                        
                        this.setStatus(9);
                    }
                    else{
                        // Keep safe distance from that player
                        new_diffX = -(nearest_big_player.getPosX() - this.getPosX());
                        new_diffY = -(nearest_big_player.getPosY() - this.getPosY());
                    }
                    
                    this.setStatus(2);
                    
                    switch(this.getType()){
                        case 3:
                            // CHICKEN (JUMP AWAY)
                            float lose = this.getScore() * this.getJumpLosingFactor();
                            if(!this.isTryingToJump() && this.getScore() - lose >= 30.0f){
                                this.isJumping();
                            }
                            break;
                        case 4:
                            // PANIC (PANIC)
                            if(!this.isTryingToPanic()){
                                this.isPanicking();
                            }
                            break;
                    }
                }
            }
            else{
                if(nearest_small_player != null){
                    if(min_small_player_distance < min_small_virus_distance || min_small_virus_distance == 0.0){
                        // Follow player
                        new_diffX = nearest_small_player.getPosX()-this.getPosX();
                        new_diffY = nearest_small_player.getPosY()-this.getPosY();

                        this.setStatus(3);

                        // Check if its worth to jump or not
                        float lose = this.getScore() * this.getJumpLosingFactor();
                        if(!this.isTryingToJump() && this.getScore() - lose >= 30.0f && nearest_small_player.getScore() - lose > 0.0f){
                            this.isJumping();
                        }
                    }
                    else{
                        // Keep safe distance from smallest virus
                        new_diffX = -(nearest_small_virus.getPosX() - this.getPosX());
                        new_diffY = -(nearest_small_virus.getPosY() - this.getPosY());

                        this.setStatus(4);
                    }
                }
                else{
                    // Calculate nearest aglomerado
                    synchronized(this.getNearAglomeradosPointer()){
                        Iterator<Aglomerado> it_aglomerados = this.getNearAglomeradosPointer().getNear().iterator();
                        while(it_aglomerados.hasNext()){
                            Aglomerado a = it_aglomerados.next();
                            double current_distance = this.euclidianDistance(a);
                            if(current_distance < min_aglomerado_distance || min_aglomerado_distance == 0.0){
                                nearest_aglomerado = a;
                                min_aglomerado_distance = current_distance;
                            }
                        }
                    }

                    if(nearest_aglomerado!=null){
                        if(min_aglomerado_distance < min_small_virus_distance || min_small_virus_distance == 0.0){
                            // Catch aglomerado
                            new_diffX = nearest_aglomerado.getPosX() - this.getPosX();
                            new_diffY = nearest_aglomerado.getPosY() - this.getPosY();

                            this.setStatus(1);
                        }
                        else{
                            // Keep safe distance from smallest virus
                            new_diffX = -(nearest_small_virus.getPosX() - this.getPosX());
                            new_diffY = -(nearest_small_virus.getPosY() - this.getPosY());

                            this.setStatus(4);
                        }
                    }
                    else{
                        // If nothing on sight, controlled random ...
                        // Calculate random position but keep it controlled from the last direction      
                        double add_angle = 0.2/this.getSize();
                        if(this.angle + add_angle >= 360.0){
                            this.angle = 360 - this.angle;
                        }

                        this.angle+=add_angle;
                        new_diffX = this.getRange()*(float) Math.cos(this.angle);
                        new_diffY = this.getRange()*(float) Math.sin(this.angle);

                        this.setStatus(0);
                    }
                }
            }

            // Update position
            dir.setDXDY(new_diffX, new_diffY);
            dir.setDistance();
            dir.normalize();
        }

        // Dumb verification
        if(dislocation < 10){
            this.incrementPosX(dir.getDX()*dislocation);
            this.incrementPosY(dir.getDY()*dislocation);
        }
    }
    
    public void validateAgentPos(float dislocation, int width, int height){
        boolean bad = false;
        boolean very_bad = false;
        if(this.getPosX() - this.getRange() < 1){
            bad = true;
        }
        if(this.getPosY() - this.getRange() < 1){
            bad = true;
        }
        if(this.getPosX() + this.getRange() > width){
            bad = true;
        }
        if(this.getPosY() + this.getRange() > height){
            bad = true;
        }
        
        if(this.getPosX() < 1){
            this.setPosX(1);
            very_bad = true;
        }
        if(this.getPosY() < 1){
            this.setPosY(1);
            very_bad = true;
        }
        if(this.getPosX() > width){
            this.setPosX(width - 1);
            very_bad = true;
        }
        if(this.getPosY() > height){
            this.setPosY(height - 1);
            very_bad = true;
        }
        
        if(very_bad){
            if(this.angle + 10.0 > 360.0){
                this.angle = 360 - this.angle;
            }
            else this.angle += 10.0;
        }
        else{
            if(bad){
                double add_angle = 0.3/this.getSize();
                
                if(this.angle + add_angle > 360.0){
                    this.angle = 360 - this.angle;
                }
                else this.angle += add_angle;
            }
        }
    }
    
    // PLAYER
    public void updatePlayerPosition(float dislocation, float x, float y){
        this.direction.setDXDY(x-this.getPosX(), y-this.getPosY());
        this.direction.setDistance();
        this.direction.normalize();
        this.incrementPosX(this.direction.getDX()*dislocation);
        this.incrementPosY(this.direction.getDY()*dislocation);
    }
    
    public void validatePlayerPos(float dislocation, int width, int height){
        if(this.getPosX() < 1) this.setPosX(1);
        if(this.getPosY() < 1) this.setPosY(1);
        if(this.getPosX() > width) this.setPosX(width-1);
        if(this.getPosY() > height) this.setPosY(height-1);
    }
    
    public void addLog(String s){
        synchronized(this.logger){
            String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
            this.logger.add("[" + time + "] " + s);
        }
    }
    
    public void addBush(Virus v){
        this.bushes.add(v);
    }
    
    public void removeBush(Virus v){
        this.bushes.remove(v);
    }
    
    //----- OVERRIDE METHODS -----//
    
    @Override
    public Player clone(){
        return new Player(this);
    }
    
    @Override
    public boolean equals(Object o){
        if(this==o) return true;
        if(o==null || this.getClass() != o.getClass()) return false;
        else{
            Player p = (Player) o;
            return super.equals(o) && 
                   this.getDirection().equals(p.getDirection()) &&
                   this.getVelocity() == p.getVelocity() &&
                   this.getType() == p.getType() &&
                   this.getRangeFactor() == p.getRangeFactor() &&
                   this.getVelocityFactor() == p.getVelocityFactor() &&
                   this.getJumpTime() == p.getJumpTime() &&
                   this.getJumpVelocity() == p.getJumpVelocity() &&
                   this.getJumpLosingFactor() == p.getJumpLosingFactor() &&
                   this.getVirusLosingFactor() == p.getVirusLosingFactor();
        }
    }
    
}
