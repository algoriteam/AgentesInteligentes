package Data;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class TextBox {
    
    //----- VARIABLES -----//
        
            // Text
            private String text;
            
            // Colors and fonts
            private Color color_line;
            private Color color_background;
            private Color color_text;
            private int font_size;
            
            // Position
            private int offX;
            private int offY;
        
    //----- CONSTRUCTORS -----//
    
    public TextBox(String t, Color cl, Color cb, Color ct, int fs, int x, int y){
        this.text = t;
        
        this.color_line = cl;
        this.color_background = cb;
        this.color_text = ct;
        this.font_size = fs;
        
        this.offX = x;
        this.offY = y;
    }
    
    //----- METHODS -----//
    
    //----- GETS -----//
    
    public String getText() {
        return this.text;
    }

    public Color getColor_line() {
        return this.color_line;
    }

    public Color getColor_background() {
        return this.color_background;
    }

    public Color getColor_text() {
        return this.color_text;
    }

    public int getFont_size() {
        return this.font_size;
    }

    public int getOffX() {
        return this.offX;
    }

    public int getOffY() {
        return this.offY;
    }
    
    //----- SETS -----//
    
    public void setColor_line(Color cl) {
        this.color_line = cl;
    }

    public void setColor_background(Color cb) {
        this.color_background = cb;
    }

    public void setColor_text(Color ct) {
        this.color_text = ct;
    }

    public void setFont_size(int fs) {
        this.font_size = fs;
    }

    public void setOffX(int x) {
        this.offX = x;
    }

    public void setOffY(int y) {
        this.offY = y;
    }

    public void setText(String t){
        this.text = t;
    }
    
    //----- OTHER METHODS -----//
    
    //----- PAINT -----//
    
    public void paint(Graphics2D g){
        Font font = new Font("TimesRoman", Font.PLAIN, this.getFont_size());
        FontMetrics metrics = g.getFontMetrics(font);
        
        int string_width = metrics.stringWidth(this.getText());
        
        // Draw background
        g.setColor(this.getColor_background());
        Rectangle.Float r1 = new Rectangle.Float(this.getOffX(), this.getOffY(), string_width + 100, 50);
        g.draw(r1);
        g.fill(r1);
        
        // Draw outerline
        g.setColor(this.getColor_line());
        Rectangle.Float r2 = new Rectangle.Float(this.getOffX(), this.getOffY(), string_width + 100, 50);
        g.draw(r2);
        
        // Draw text
        g.setColor(this.getColor_text());
        g.setFont(font);
        g.drawString(this.getText(), this.getOffX() + 50, this.getOffY()+ ((50 - metrics.getHeight()) * 0.5f) + metrics.getAscent());
    }
    
}
