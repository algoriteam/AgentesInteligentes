package Data;

import java.awt.Color;
import java.io.Serializable;

public class Virus extends Dot implements Serializable{
    
    //----- VARIABLES -----//
    
            // NONE
    
    //----- CONSTRUCTORS -----//
   
    public Virus(String name, float posX, float posY, int s, Color color) {
        super(name, posX, posY, s, s, color);
    }
    
    public Virus(Virus a) {
        super(a);
    }
    
    //----- OVERRIDE METHODS -----//
    
    @Override
    public Virus clone(){
        return new Virus(this);
    }
    
    @Override
    public boolean equals(Object o){
        if(this==o) return true;
        if(o==null || this.getClass() != o.getClass()) return false;
        else{
            return super.equals(o);
        }
    }
    
}
