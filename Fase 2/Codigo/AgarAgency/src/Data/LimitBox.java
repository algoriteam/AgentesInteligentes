package Data;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class LimitBox {
    
    //----- VARIABLES -----//
        
            // Color
            private Color color_line;
            
            // Size
            private int width;
            private int height;
        
    //----- CONSTRUCTORS -----//
    
    public LimitBox(Color cl, int w, int h){
        this.color_line = cl;

        this.width = w;
        this.height = h;
    }
    
    //----- METHODS -----//
    
    //----- GETS -----//

    public Color getColor_line() {
        return this.color_line;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }
    
    //----- SETS -----//
    
    public void setColor_line(Color cl) {
        this.color_line = cl;
    }

    public void setWidth(int w) {
        this.width = w;
    }

    public void setOffY(int h) {
        this.height = h;
    }
    
    //----- OTHER METHODS -----//
    
    //----- PAINT -----//
    
    public void paint(Graphics2D g){
        // Draw limits
        g.setColor(this.getColor_line());
        Rectangle.Float r = new Rectangle.Float(0, 0, this.width, this.height);
        g.draw(r);

    }
    
}
