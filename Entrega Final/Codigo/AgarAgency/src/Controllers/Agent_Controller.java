package Controllers;

import Data.Aglomerado;
import Data.Crono;
import Data.LimitBox;
import Data.Player;
import Data.Vetor;
import Data.Virus;
import Presentation.AgentScreen;
import Presentation.GameMaster;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class Agent_Controller extends JPanel{
    
    // ----- VARIABLES ----- //
        
        // GameMaster
        private GameMaster gm;
        
        // Father
        private AgentScreen father;
        
        // Frame control
        private double zoom = 1.0;
        private Graphics2D graphics_main;
        private RenderingHints image_antialising;

        // Graphics Thread
        private LimitBox limit;
        private Thread paint_thread;
        private Thread frame_thread;
        private Thread jump_thread;
        private Thread panic_thread;

        // Frame control
        private int frames;

        // Player
        private Player me;
        private GuiAgent agent;
        
        // Calculate dislocation from elapsed time
        private Crono time;
        private float dislocation;
        private int jump_elapsed_time;
        private int panic_elapsed_time;
    
    // ----- CONSTRUCTORS ----- //
        
    public Agent_Controller(GameMaster m, AgentScreen f, GuiAgent ag, Player pl){
        this.gm = m;
        this.father = f;
        this.agent = ag;
        this.me = pl;
        
        this.limit = new LimitBox(Color.BLACK, this.gm.getTableWidth(), this.gm.getTableHeight());
        this.paint_thread = new Thread(new Agent_Controller.paintAll());
        this.frame_thread = new Thread(new Agent_Controller.frameCounter());
        this.jump_thread = new Thread(new Agent_Controller.impulsePlayer());
        this.panic_thread = new Thread(new Agent_Controller.panicPlayer());
        this.frames = 0;
        this.time = new Crono();
        this.jump_elapsed_time = 0;
        this.panic_elapsed_time = 0;
        init();
    }
    
    // ----- METHODS ----- //
    
    private void init(){
        this.setBackground(Color.WHITE);
        this.setPreferredSize(new Dimension(this.gm.getTableWidth(), this.gm.getTableHeight()));

        setFocusable(true);
        requestFocusInWindow();
        
        this.image_antialising = new RenderingHints(
        RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

        this.addMouseWheelListener(new Agent_Controller.MyMouseWheelListener());
        this.addMouseMotionListener(new Agent_Controller.MyMouseMotionListener());
        this.addMouseListener(new Agent_Controller.MyMouseListener());
        
        this.paint_thread.start();
        this.frame_thread.start();
    }
    
    @Override
    public void paint(Graphics gr){
        super.paintComponent(gr);
        
        this.graphics_main = (Graphics2D) gr.create();
        AffineTransform at = this.graphics_main.getTransform();
        
        // ANTIALISING?
        if(this.gm.getAntialiasing()) this.graphics_main.setRenderingHints(this.image_antialising);
        
        // CONTROL PLAYER POSITION (DONT USE MOUSE)
        this.me.updateVelocity();
        this.dislocation = this.me.getVelocity()*this.time.stop_milli();

            // Calculate new point
            this.me.updateAgentPosition(this.dislocation);
            this.me.validateAgentPos(this.dislocation, this.gm.getTableWidth(), this.gm.getTableHeight());
        
        // CENTER ON PLAYER
        at.translate(this.getWidth() * 0.5 - this.me.getPosX() * this.zoom, this.getHeight() * 0.5 - this.me.getPosY() * this.zoom);
        at.scale(this.zoom, this.zoom);
        this.graphics_main.transform(at);
        
        // PAINT LIMITS
        this.limit.paint(this.graphics_main);
        
        // PAINT AGLOMERADOS
        synchronized(this.me.getNearAglomeradosPointer()){
            Iterator<Aglomerado> it_aglomerados = this.me.getNearAglomeradosPointer().getNear().iterator();
            while(it_aglomerados.hasNext()){
                Aglomerado a = it_aglomerados.next();
                if(this.me.killed(a)){
                    // SEND GUI EVENT TO REMOVE AGLOMERADO
                    GuiEvent ge = new GuiEvent(a.getName(),3);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.addLog("Aglomerado +" + a.getScore() + " Points");
                    this.me.incrementScore(a.getScore());
                }
                a.paint_aglomerado(this.graphics_main);
            }
        }
        
        // PAINT VIRUS
        synchronized(this.me.getNearVirusPointer()){
            Iterator<Virus> it_virus = this.me.getNearVirusPointer().getNear().iterator();
            while(it_virus.hasNext()){
                Virus v = it_virus.next();
                if(this.me.getScore() > 30 && this.me.killed(v) && v.getSize() < this.me.getSize()){
                    // SEND GUI EVENT TO REMOVE VIRUS
                    GuiEvent ge = new GuiEvent(v.getName(),2);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.breakInVirus();
                }
                v.paint_virus(this.graphics_main);
            }
        }

        // PAINT PLAYERS
        synchronized(this.me.getNearPlayersPointer()){
            Iterator<Player> it_players = this.me.getNearPlayersPointer().getNear().iterator();
            while(it_players.hasNext()){
                Player p = it_players.next();
                if(this.me.getScore() > 30 && this.me.killed(p) && p.getSize() < this.me.getSize() && !p.getName().equals(this.me.getName())){
                    // SEND GUI EVENT TO REMOVE PLAYER
                    GuiEvent ge = new GuiEvent(p.getName(),1);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.addLog("Player +" + p.getScore() + " points");
                    father.setAgent_status(6);
                    this.me.incrementScore(p.getScore());
                }
                p.paint_player(this.graphics_main, p.getDirection(), p.getRange());
            }
        }
        
        // PAINT PLAYER
        this.me.paint_player(this.graphics_main, this.me.getDirection(), this.me.getRange());
        
        // DISPOSE GRAPHICS
        this.graphics_main.dispose();
        this.frames++;
        
        this.time.start();
    }
    
    public void calculate(){
        
        // PAINT AGLOMERADOS
        synchronized(this.me.getNearAglomeradosPointer()){
            Iterator<Aglomerado> it_aglomerados = this.me.getNearAglomeradosPointer().getNear().iterator();
            while(it_aglomerados.hasNext()){
                Aglomerado a = it_aglomerados.next();
                if(this.me.killed(a)){
                    // SEND GUI EVENT TO REMOVE AGLOMERADO
                    GuiEvent ge = new GuiEvent(a.getName(),3);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.addLog("Aglomerado +" + a.getScore() + " Points");
                    this.me.incrementScore(a.getScore());
                }
            }
        }
        
        // PAINT VIRUS
        synchronized(this.me.getNearVirusPointer()){
            Iterator<Virus> it_virus = this.me.getNearVirusPointer().getNear().iterator();
            while(it_virus.hasNext()){
                Virus v = it_virus.next();
                if(this.me.getScore() > 30 && this.me.killed(v) && v.getSize() < this.me.getSize()){
                    // SEND GUI EVENT TO REMOVE VIRUS
                    GuiEvent ge = new GuiEvent(v.getName(),2);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.breakInVirus();
                }
            }
        }

        // PAINT PLAYERS
        synchronized(this.me.getNearPlayersPointer()){
            Iterator<Player> it_players = this.me.getNearPlayersPointer().getNear().iterator();
            while(it_players.hasNext()){
                Player p = it_players.next();
                if(this.me.getScore() > 30 && this.me.killed(p) && p.getSize() < this.me.getSize() && !p.getName().equals(this.me.getName())){
                    // SEND GUI EVENT TO REMOVE PLAYER
                    GuiEvent ge = new GuiEvent(p.getName(),1);
                    this.agent.postGuiEvent(ge);
                    
                    this.me.addLog("Player +" + p.getScore() + " points");
                    father.setAgent_status(6);
                    this.me.incrementScore(p.getScore());
                }
            }
        }
        
        // CONTROL PLAYER POSITION (DONT USE MOUSE)
        this.me.updateVelocity();
        this.dislocation = this.me.getVelocity() * this.time.stop_milli();

        // Calculate new point
        this.me.updateAgentPosition(this.dislocation);
        this.me.validateAgentPos(this.dislocation, this.gm.getTableWidth(), this.gm.getTableHeight());
        
        this.time.start();
    }
    
    // ----- RUNNABLES ---- //
    
    class paintAll implements Runnable {
        @Override
        public void run() {
            while(true){
                if(father.isVisible()){
                    revalidate();
                    repaint();
                }
                else{
                    calculate();
                }
                
                // CHECK IF AGENT IS JUMPING
                if(me.isTryingToJump() && !jump_thread.isAlive()){
                    jump_thread = new Thread(new Agent_Controller.impulsePlayer());
                    jump_thread.start();
                }
                
                // CHECK IF AGENT IS PANICKING
                if(me.isTryingToPanic() && !panic_thread.isAlive()){
                    panic_thread = new Thread(new Agent_Controller.panicPlayer());
                    panic_thread.start();
                }
                
                // SET AGENT STATUS
                father.setAgent_status(me.getStatus());
                
                try{
                    Thread.sleep(gm.getRefresh_rate());
                }catch(InterruptedException e){
                    System.out.println("[AGENTS] Paint - > Thread.sleep error!");
                }
            }
        }
    }
    
    class frameCounter implements Runnable {
        @Override
        public void run() {
            while(true){
                father.setFrame_counter(""+frames);
                frames = 0;
                
                father.setAgent_score(""+me.getScore());
                father.setAgent_log(me.getLog());
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    System.out.println("[AGENTS] Frame counter - > Thread.sleep error!");
                }
            }
        }
    }
    
    class impulsePlayer implements Runnable {
        @Override
        public void run() {
            me.breakInJump();
            while(jump_elapsed_time < gm.getTick_rate() * me.getJumpTime()){
                jump_elapsed_time++;
                me.setVelocityFactor(me.getJumpVelocity());
                
                try{
                    Thread.sleep(gm.getRefresh_rate());
                }catch(InterruptedException e){
                    System.out.println("[AGENTS] Impulse - > Thread.sleep error!");
                }
            }
            jump_elapsed_time = 0;
            me.notJumping();
        }
    }
    
    class panicPlayer implements Runnable {
        @Override
        public void run() {
            while(panic_elapsed_time < gm.getTick_rate() * me.getPanicTime()){
                panic_elapsed_time++;   
                // Update position
                Vetor dir = me.getDirection();
                dir.setDXDY(me.getRange()*(float) Math.cos(panic_elapsed_time), me.getRange()*(float) Math.sin(panic_elapsed_time));
                dir.setDistance();
                dir.normalize();
                try{
                    Thread.sleep(gm.getRefresh_rate());
                }catch(InterruptedException e){
                    System.out.println("[AGENTS] Panic - > Thread.sleep error!");
                }
            }
            panic_elapsed_time = 0;
            me.notPanicking();
            
            try{
                Thread.sleep(2000);
            }catch(InterruptedException e){
                System.out.println("[AGENTS] Cant panic now - > Thread.sleep error!");
            }
        }
    }
    
    // ----- MOUSE ADAPTER ---- //
    
    class MyMouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e){
            if(SwingUtilities.isLeftMouseButton(e)){
                
            }
            else{
                if(SwingUtilities.isRightMouseButton(e)){
                    // SEND GUI EVENT TO REMOVE PLAYER
                    GuiEvent ge = new GuiEvent(me.getName(),1);
                    agent.postGuiEvent(ge);
                }
                else{
                    if(SwingUtilities.isMiddleMouseButton(e)){
                        
                    }
                }
            }
        }
    }
    
    class MyMouseMotionListener extends MouseAdapter {
        @Override
        public void mouseMoved(MouseEvent e) {

        }
    }
    
    class MyMouseWheelListener extends MouseAdapter {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e){
            double zoomFactor = -0.05 * e.getPreciseWheelRotation() * zoom;
            if(zoom + zoomFactor >= 0.2 && zoom + zoomFactor <= 15.0){
                zoom += zoomFactor;
            }
        }
    }
    
    // ----- OTHER METHODS ---- //
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(this.gm.getTableWidth(), this.gm.getTableHeight());
    };
    
}
