package Services;

import Behaviours.PlayerManagement;
import Behaviours.SendLeadPlayers;
import Behaviours.SendNearPlayers;
import Presentation.GameMaster;
import jade.core.Agent;
import jade.core.behaviours.ParallelBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;


public class Service_Players extends Agent{
    
    //----- VARIABLES -----//
    
            private GameMaster gm;
            
    //----- METHODS -----//
            
    @Override
    protected void setup(){
        super.setup();
        
        // Receive objects from input arguments
        Object[] args = getArguments();
        if(args != null && args.length > 0){
            this.gm = (GameMaster) args[0];
        }
        
        DFAgentDescription df = new DFAgentDescription();
        df.setName(this.getAID());
        
        ServiceDescription sd = new ServiceDescription();
        sd.setType("gamemaster_services");
        sd.setName("service_players");
        df.addServices(sd);
        
        try{
            DFService.register(this, df);
            System.out.println("[PLATFORM-CONTROLLER] Successfully started Service '" + this.getLocalName() + "'");
            
            ParallelBehaviour pb = new ParallelBehaviour(this, ParallelBehaviour.WHEN_ALL){
                
                @Override
                public int onEnd(){
                    myAgent.doDelete();
                    return 0;
                }
                
            };
            
            pb.addSubBehaviour(new SendNearPlayers(this, this.gm.getRefresh_rate(), this.gm));
            pb.addSubBehaviour(new SendLeadPlayers(this, this.gm.getRefresh_rate(), this.gm));
            pb.addSubBehaviour(new PlayerManagement(this.gm));
            this.addBehaviour(pb);
        }
        catch(FIPAException e){
            System.out.println("[PLATFORM-CONTROLLER] Error registing Service '" + this.getLocalName() + "'");
        }
    }
    
    @Override
    protected void takeDown(){
        super.takeDown();
        
        try{
            DFService.deregister(this);
            System.out.println("[PLATFORM-CONTROLLER] Successfully stopped Service '" + this.getLocalName() + "'");
        }
        catch(FIPAException e){
            System.out.println("[PLATFORM-CONTROLLER] Error un-registing Service '" + this.getLocalName() + "'");
        }
    }
    
}
