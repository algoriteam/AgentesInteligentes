package Packages;

import Data.Player;
import java.io.Serializable;

public class SimplePlayer implements Serializable{
    
    //----- VARIABLES -----//
    
    private String name;
    
    private float posX;
    private float posY;
    
    private int type;
    
    private int score;
    
    //----- CONSTRUCTORS -----//
    
    public SimplePlayer(Player p) {
        this.name = p.getName();
        this.posX = p.getPosX();
        this.posY = p.getPosY();
        this.type = p.getType();
        this.score = p.getScore();
    }
    
    //----- METHODS -----//

    public String getName() {
        return this.name;
    }

    public float getPosX() {
        return this.posX;
    }

    public float getPosY() {
        return this.posY;
    }
    
    public int getType() {
        return this.type;
    }

    public int getScore() {
        return score;
    }

    public void setName(String n) {
        this.name = n;
    }

    public void setPosX(float x) {
        this.posX = x;
    }

    public void setPosY(float y) {
        this.posY = y;
    }
    
    public void setType(int t) {
        this.type = t;
    }

    public void setScore(int s) {
        this.score = s;
    }

}
