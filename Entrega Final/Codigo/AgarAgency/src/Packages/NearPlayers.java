package Packages;

import Data.Player;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.Iterator;

public class NearPlayers implements Serializable{
    
    //----- VARIABLES -----//
    
            private ArrayList<Player> near_players;
    
    //----- CONSTRUCTORS -----//
    
    public NearPlayers() {
        this.near_players = new ArrayList<>();
    }
    
    public NearPlayers(NearPlayers p) {
        this.near_players = new ArrayList<>();
        Iterator<Player> it_players = p.getNear().iterator();
        while(it_players.hasNext()){
            Player c = it_players.next();
            this.near_players.add(c.clone());
        }
    }
    
    //----- METHODS -----//

    public ArrayList<Player> getNear() {
        return this.near_players;
    }
    
    //----- OTHER METHODS -----//
    
    public void addNear(Player p){
        this.near_players.add(p);
    }
    
    @Override
    public NearPlayers clone(){
        return new NearPlayers(this);
    }

}
