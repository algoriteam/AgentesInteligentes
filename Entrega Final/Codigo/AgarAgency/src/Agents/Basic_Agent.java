package Agents;

import Behaviours.AgentReceiveAllPackages;
import Behaviours.SendPlayerStatus;
import Data.Player;
import Presentation.AgentScreen;
import Presentation.GameMaster;
import jade.core.AID;
import jade.core.behaviours.ParallelBehaviour;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import java.io.IOException;


public class Basic_Agent extends GuiAgent{
    
    //----- VARIABLES -----//
    
        // GameMaster
        private GameMaster gm;
        
        // Player
        private AgentScreen agent_screen;
        private Player me;
        
    //----- METHODS -----//

    @Override
    protected void setup(){
        super.setup();
        System.out.println("[AGENTS] Agent '"+ this.getLocalName() + "' joined the party");
        
        Object[] args = getArguments();
        if(args != null && args.length > 0){
            this.gm = (GameMaster) args[0];
            this.me = (Player) args[1];
            
            this.agent_screen = new AgentScreen(this.gm, this, this.me);
            
            this.gm.getAgentsPlatform().addAgentWindow(me.getName(), this.agent_screen);
            
            ParallelBehaviour pb = new ParallelBehaviour(this, ParallelBehaviour.WHEN_ALL){
                
                @Override
                public int onEnd(){
                    myAgent.doDelete();
                    return 0;
                }
                
            };
            
            pb.addSubBehaviour(new AgentReceiveAllPackages(this.gm, this.agent_screen, this.me));
            pb.addSubBehaviour(new SendPlayerStatus(this, this.gm.getMessage_tick(), this.me));
            this.addBehaviour(pb);
        }
    }

    
    @Override
    protected void takeDown(){
        super.takeDown();
        System.out.println("[AGENTS] Agent '"+ this.getLocalName() + "' left the party");
    }

    @Override
    protected void onGuiEvent(GuiEvent ge) {  
        int command = ge.getType();
        String remove_name = (String) ge.getSource();
        AID receiver = new AID();
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        
        switch(command){
            // REMOVE PLAYER
            case 1:
                try{
                    // Set receiver
                    receiver.setLocalName("service_players");

                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("32");
                    msg.setContentObject(remove_name);

                    // Send message
                    this.send(msg);
                }
                catch(IOException e){
                    System.out.println("[AGENTS] Agent '" + this.getLocalName() + "' couldn't send player name to remove");
                }
                break;
            // REMOVE VIRUS
            case 2:
                try{
                    // Set receiver
                    receiver.setLocalName("service_virus");

                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("22");
                    msg.setContentObject(remove_name);

                    // Send message
                    this.send(msg);
                }
                catch(IOException e){
                    System.out.println("[AGENTS] Agent '" + this.getLocalName() + "' couldn't send virus name to remove");
                }
                break;
            // REMOVE AGLOMERADO
            case 3:
                try{
                    // Set receiver
                    receiver.setLocalName("service_aglomerados");

                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("12");
                    msg.setContentObject(remove_name);

                    // Send message
                    this.send(msg);
                }
                catch(IOException e){
                    System.out.println("[AGENTS] Agent '" + this.getLocalName() + "' couldn't send aglomerado name to remove");
                }
                break;
            default:
                System.out.println("[AGENTS] Invalid command stated on GuiEvent - (ID = " + command + ")");
                break;
        }
    }
}
