package Data;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.io.Serializable;
import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Dot implements Serializable{
    
    //----- VARIABLES -----//
            
            // Name
            private String name;
            
            // Position
            private float posX;
            private float posY;
            
            // Starting size and score
            private int starting_size;
            private int score;
            
            // Color
            private Color color;
    
    //----- CONSTRUCTORS -----//

    public Dot(String n, float posX, float posY, int ss, int s, Color c){
        this.name = n;
        this.posX = posX;
        this.posY = posY;
        this.starting_size = ss;
        this.score = s;
        this.color = c;
    }

    public Dot(Dot d){
        this.name = d.getName();
        this.posX = d.getPosX();
        this.posY = d.getPosY();
        this.starting_size = d.getStartingSize();
        this.score = d.getScore();
        this.color = d.getColor();
    }
    
    //----- METHODS -----//
    
    //----- GETS -----//
    
    public String getName() {
        return this.name;
    }

    public float getPosX() {
        return this.posX;
    }

    public float getPosY() {
        return this.posY;
    }
    
    public int getStartingSize() {
        return this.starting_size;
    }
    
    public int getScore() {
        return this.score;
    }

    public Color getColor() {
        return this.color;
    }
    
    public float getSize() {
        return this.starting_size + this.getScore() * 0.04f;
    }
    
    //----- SETS -----//
    
    public void setName(String n) {
        this.name = n;
    }

    public void setPosX(float x) {
        this.posX = x;
    }

    public void setPosY(float y) {
        this.posY = y;
    }

    public void setStartingSize(int s) {
        this.starting_size = s;
    }
    
    public void setScore(int s) {
        this.score = s;
    }

    public synchronized void setColor(Color c) {
        this.color = c;
    }
    
    //----- OTHER METHODS -----//
    
    //----- SCORE -----//
    
    public void incrementScore(int i){
        this.score+=i;
    }
    
    public void decrementScore(int i){
        if(this.getScore() - i >= 30) this.setScore(this.getScore() - i);
        else this.setScore(this.getStartingSize());
    }
    
    //----- POSITION -----//
    
    public void incrementPosX(float a){
        this.setPosX(this.getPosX() + a);
    }
    
    public void incrementPosY(float a){
        this.setPosY(this.getPosY() + a);
    }
    
    //----- DISTANCE -----//
    
    public double manhattanDistance(Dot d){
        return abs(d.getPosX() - this.getPosX()) + abs(d.getPosY() - this.getPosY());
    }
    
    public double manhattanDistance(int x, int y){
        return abs(x - this.getPosX()) + abs(y - this.getPosY());
    }
    
    public double euclidianDistance(Dot d){
        return sqrt(pow(d.getPosX() - this.getPosX(),2) + pow(d.getPosY() - this.getPosY(),2));
    }
    
    public double euclidianDistance(int x, int y){
        return sqrt(pow(x - this.getPosX(),2) + pow(y - this.getPosY(),2));
    }
    
    //----- KILL/SIGHT -----//
    
    public boolean killed (Dot d){
        return this.euclidianDistance(d) <= this.getSize() * 0.5;
    }
    
    public boolean sights (Dot d, float range){
        return this.euclidianDistance(d) - range <= d.getSize() * 0.5;
    }
    
    public boolean sights (int x, int y, float range){
        return this.euclidianDistance(x,y) - range <= 1;
    }
    
    //----- PAINT -----//
    
    public void paint_aglomerado(Graphics2D g){
        // Draw shape
        g.setColor(this.getColor());
        
        Ellipse2D r = new Ellipse2D.Float(this.getPosX() - this.getSize() * 0.5f, this.getPosY() - this.getSize() * 0.5f, this.getSize(), this.getSize());
        g.draw(r);
        g.fill(r);
        
        g.setColor(this.getColor().darker());
        g.draw(r);
    }
    
    public void paint_virus(Graphics2D g){
        // Draw shape
        g.setColor(this.getColor());
        
        Ellipse2D r = new Ellipse2D.Float(this.getPosX() - this.getSize() * 0.5f, this.getPosY() - this.getSize() * 0.5f, this.getSize(), this.getSize());
        g.draw(r);
        g.fill(r);
        
        g.setColor(this.getColor().darker());
        g.draw(r);
        
        // Draw text
        Font font = new Font("TimesRoman", Font.PLAIN, Math.round(this.getSize()));
        FontMetrics metrics = g.getFontMetrics(font);
        
        float x = (this.getSize() - metrics.stringWidth("X")) * 0.5f;
        float y = ((this.getSize() - metrics.getHeight()) * 0.5f) + metrics.getAscent();

        g.setColor(Color.BLACK);
        g.setFont(font);
        g.drawString("X", this.getPosX() - this.getSize() * 0.5f + x, this.getPosY() - this.getSize() * 0.5f + y);
    }
    
    public void paint_player(Graphics2D g, Vetor v, float range){
        float vetorX = this.getPosX() + v.getDX() * range;
        float vetorY = this.getPosY() + v.getDY() * range;
        
        // Draw shape
        g.setColor(this.getColor());
        
        Ellipse2D r1 = new Ellipse2D.Float(this.getPosX() - this.getSize() * 0.5f, this.getPosY() - this.getSize() * 0.5f, this.getSize(), this.getSize());
        Ellipse2D r2 = new Ellipse2D.Float(this.getPosX() - range, this.getPosY() - range, 2 * range, 2 * range);
        Ellipse2D r3 = new Ellipse2D.Float(vetorX - 5, vetorY - 5, 10, 10);
        Line2D.Float main_line = new Line2D.Float(this.getPosX(), this.getPosY(), vetorX, vetorY);
        
        g.draw(r1);
        g.fill(r1);
        
        g.setColor(this.getColor().darker());
        g.draw(r1);
        g.draw(r2);
        g.draw(r3);
        g.draw(main_line);
        
        // Draw text
        Font font = new Font("TimesRoman", Font.PLAIN, Math.round(this.getSize() * 0.3f));
        FontMetrics metrics = g.getFontMetrics(font);
        
        float x = (this.getSize() - metrics.stringWidth(this.getName())) * 0.5f;
        float y = ((this.getSize() - metrics.getHeight()) * 0.5f) + metrics.getAscent();

        g.setColor(Color.BLACK);
        g.setFont(font);
        g.drawString(this.getName(), this.getPosX() - this.getSize() * 0.5f + x, this.getPosY() - this.getSize() * 0.5f + y);
    }
    
    //----- OVERRIDE METHODS -----//
    
    @Override
    public Dot clone(){
        return new Dot(this);
    }
    
    @Override
    public boolean equals(Object o){
        if(this==o) return true;
        if(o==null || this.getClass() != o.getClass()) return false;
        else{
            Dot d = (Dot) o;
            return this.getName().equals(d.getName()) &&
                   this.getPosX() == d.getPosX() &&
                   this.getPosY() == d.getPosY() &&
                   this.getStartingSize() == d.getStartingSize() &&
                   this.getScore() == d.getScore() &&
                   this.getColor().equals(d.getColor());
        }
    }
    
}   
