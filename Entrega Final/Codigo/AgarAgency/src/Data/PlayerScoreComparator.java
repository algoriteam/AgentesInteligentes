package Data;

import java.util.Comparator;

public class PlayerScoreComparator implements Comparator<Player>{
    
    //----- OVERRIDE METHODS -----//
    
    @Override
    public int compare(Player p1, Player p2) {
        return p2.getScore() - p1.getScore();
    }
    
}
