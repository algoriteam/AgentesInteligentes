package Data;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class Petri{
    
    //----- VARIABLES -----//
    
            // Size
            private int width;
            private int height;

            // Aglomerados
            private HashMap<String, Aglomerado> aglomerados;
            private int max_aglomerados;
            private int current_aglomerados;
            private int new_player_aglomerados_amount = 100;
            private int last_aglomerado_index;

            // Virus
            private HashMap<String, Virus> virus;
            private int max_virus;
            private int current_virus;
            private int new_player_virus_amount = 3;
            private int last_virus_index;

            // Players
            private HashMap<String, Player> players;
            private int last_player_index;
    
    //----- CONSTRUCTORS -----//
        
    public Petri(int w, int h, int max_agl, int max_vir, int n_p_agl, int n_p_vir){
        this.width = w;
        this.height = h;
        
        this.aglomerados = new HashMap<>();
        this.max_aglomerados = max_agl;
        this.current_aglomerados = 0;
        this.new_player_aglomerados_amount = n_p_agl;
        this.last_aglomerado_index = 1;
        
        this.virus = new HashMap<>();
        this.max_virus = max_vir;
        this.current_virus = 0;
        this.new_player_virus_amount = n_p_vir;
        this.last_virus_index = 1;
        
        this.players = new HashMap<>();
        this.last_player_index = 1;
    } 
    
    //----- METHODS -----//
    
    //----- SIZE MANAGEMENT -----//
    
    public int getWidth(){
        return this.width;
    }
    
    public int getHeight(){
        return this.height;
    }
    
    //----- AGLOMERADOS MANAGEMENT -----//

    public HashMap<String, Aglomerado> getAglomeradosPointer(){
        return this.aglomerados;
    }
    
    public int getAglomeradosMax() {
        return this.max_aglomerados;
    }
    
    public int getAddPlayerAglomeradosAmount(){
        return this.new_player_aglomerados_amount;
    }
    
    public int getAglomeradosCurrent() {
        return this.current_aglomerados;
    }
    
    public void setAglomeradosCurrent(int a) {
        this.current_aglomerados = a;
    }
    
    public void addPlayerAglomerados() {
        this.current_aglomerados += this.getAddPlayerAglomeradosAmount();
    }
    
    public int getAglomeradosNumber() {
        synchronized(this.aglomerados){
            return this.aglomerados.size();
        }
    }
    
    public void addAglomerado() {
        synchronized(this.aglomerados){
            Random rand = new Random();
            int x = rand.nextInt(this.width);
            int y = rand.nextInt(this.height);
            int size = rand.nextInt(5) + 8;
            
            int r = rand.nextInt(156) + 100;
            int g = rand.nextInt(156) + 100;
            int b = rand.nextInt(156) + 100;
            Color color = new Color(r,g,b);
            
            String key = "A"+this.last_aglomerado_index;
            Aglomerado a = new Aglomerado(key, x, y, size, color);
        
            this.aglomerados.put(key, a);
            this.last_aglomerado_index++;
        }
    }
    
    public void removeAglomerado() {
        synchronized(this.aglomerados){
            Iterator<String> it_aglomerados = this.aglomerados.keySet().iterator();
            it_aglomerados.next();
            it_aglomerados.remove();
        }
    }
    
    public void removeAglomerado(String name) {
        synchronized(this.aglomerados){
            this.aglomerados.remove(name);
        }
    }
    
    //----- VIRUS MANAGEMENT -----//
    
    public HashMap<String, Virus> getVirusPointer(){
        return this.virus;
    }
    
    public int getVirusMax() {
        return this.max_virus;
    }
    
    public int getAddPlayerVirusAmount(){
        return this.new_player_virus_amount;
    }
    
    public int getVirusCurrent() {
        return this.current_virus;
    }
    
    public void setVirusCurrent(int v) {
        this.current_virus = v;
    }
    
    public void addPlayerVirus() {
        this.current_virus += this.getAddPlayerVirusAmount();
    }
    
    public int getVirusNumber() {
        synchronized(this.virus){
            return this.virus.size();
        }
    }
    
    public void addVirus() {
        synchronized(this.virus){
            Random rand = new Random();
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);
            int size = rand.nextInt(30) + 80;

            Color color = new Color(100,255,100);
            String key = "V"+this.last_virus_index;
            Virus v = new Virus(key, x, y, size, color);

            this.virus.put(key, v);
            this.last_virus_index++;
        }
    }
    
    public void removeVirus() {
        synchronized(this.virus){
            Random rand = new Random();
            int size = this.getVirusNumber();
            int index = rand.nextInt(size) + 1;
            Iterator<String> it_virus = this.virus.keySet().iterator();
            for(int i = 0; i<index; i++) it_virus.next();
            it_virus.remove();
        }
    }
    
    public void removeVirus(String name) {
        synchronized(this.virus){
            this.virus.remove(name);
        }
    }
    
    //----- PLAYER MANAGEMENT -----//
    
    public Player getPlayer(String name){
        synchronized(this.players){
            return this.players.get(name);
        }
    }
    
    public HashMap<String, Player> getPlayersPointer(){
        return this.players;
    }
    
    public int getPlayersAgentsNumber() {
        synchronized(this.players){
            return this.players.size();
        }
    }
    
    public int getPlayersNumber() {
        synchronized(this.players){
            int counter = 0;
            Iterator<String> it_players = this.players.keySet().iterator();
            while(it_players.hasNext()){
                Player p = this.players.get(it_players.next());
                if(!p.isAgent()) counter++;
            }
            return counter;
        }
    }
    
    public int getAgentsNumber() {
        synchronized(this.players){
            int counter = 0;
            Iterator<String> it_players = this.players.keySet().iterator();
            while(it_players.hasNext()){
                Player p = this.players.get(it_players.next());
                if(p.isAgent()) counter++;
            }
            return counter;
        }
    }
    
    public Player addPlayer(){
        synchronized(this.players){
            Random rand = new Random();
            int x = rand.nextInt(this.width);
            int y = rand.nextInt(this.height);
            int size = 30;

            int r = rand.nextInt(156) + 100;
            int g = rand.nextInt(156) + 100;
            int b = rand.nextInt(156) + 100;
            Color color = new Color(r,g,b);

            String name = "Player "+this.last_player_index;
            Player p = new Player(name, x, y, size, color, -1);

            this.players.put(name, p);
            this.last_player_index++;

            // ADD MORE AGLOMERADOS
            if(this.getAglomeradosCurrent() + this.getAddPlayerAglomeradosAmount() <= this.getAglomeradosMax()) addPlayerAglomerados();

            // ADD MORE VIRUS
            if(this.getVirusCurrent() + this.getAddPlayerVirusAmount() <= this.getVirusMax()) addPlayerVirus();
            
            System.out.println("[PETRI] Add Player '" + name + "' on position("+ x +","+ y +")");
            
            return p;
        }
    }
    
    public Player addAgent(){
        synchronized(this.players){
            Random rand = new Random();
            int x = rand.nextInt(this.width);
            int y = rand.nextInt(this.height);
            int size = 30;

            int r = rand.nextInt(156) + 100;
            int g = rand.nextInt(156) + 100;
            int b = rand.nextInt(156) + 100;
            Color color = new Color(r,g,b);

            String name = "Agent "+this.last_player_index;
            Player a = new Player(name, x, y, size, color, 0);

            this.players.put(name, a);
            this.last_player_index++;

            // ADD MORE AGLOMERADOS
            if(this.getAglomeradosCurrent() + this.getAddPlayerAglomeradosAmount() <= this.getAglomeradosMax()) addPlayerAglomerados();

            // ADD MORE VIRUS
            if(this.getVirusCurrent() + this.getAddPlayerVirusAmount() <= this.getVirusMax()) addPlayerVirus();
            
            System.out.println("[PETRI] Add Agent '" + name + "' on position (" + x + "," + y + ")");
            
            return a;
        }
    }
    
    public void removePlayer(String name){
        synchronized(this.players){
            if(this.players.containsKey(name)){
                this.players.remove(name);
                System.out.println("[PETRI] Remove player/agent '" + name + "'");

                // REMOVE AGLOMERADOS
                int num_aglomerados = this.getPlayersAgentsNumber() * 100;
                if(num_aglomerados <= this.getAglomeradosMax()) this.setAglomeradosCurrent(num_aglomerados);
                else this.setAglomeradosCurrent(this.getAglomeradosMax());

                // REMOVE VIRUS
                int num_virus = this.getPlayersAgentsNumber() * 3;
                if(num_virus <= this.getVirusMax()) this.setVirusCurrent(num_virus);
                else this.setVirusCurrent(this.getVirusMax());
            }
        }
    }

}
