package Data;

import java.awt.Color;
import java.io.Serializable;

public class Aglomerado extends Dot implements Serializable{
    
    //----- VARIABLES -----//
    
            // NONE
    
    //----- CONSTRUCTORS -----//
    
    public Aglomerado(String name, float posX, float posY, int s, Color color) {
        super(name ,posX, posY, s, s, color);
    }
    
    public Aglomerado(Aglomerado a) {
        super(a);
    }
   
    //----- OVERRIDE METHODS -----//
    
    @Override
    public Aglomerado clone(){
        return new Aglomerado(this);
    }
    
    @Override
    public boolean equals(Object o){
        if(this==o) return true;
        if(o==null || this.getClass() != o.getClass()) return false;
        else{
            return super.equals(o);
        }
    }
    
}
