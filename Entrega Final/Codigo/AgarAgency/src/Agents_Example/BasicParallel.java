/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agents_Example;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;


public class BasicParallel extends Agent{
    
    @Override
        protected void setup(){
            super.setup();
            System.out.println(this.getLocalName() + " starting!");
            
            ParallelBehaviour b = new ParallelBehaviour(this, ParallelBehaviour.WHEN_ALL){
                
                @Override
                public int onEnd(){
                    System.out.println("ParallelBehaviour ending!");
                    myAgent.doDelete();
                    return 0;
                }
                
            };
            
            this.addBehaviour(b);
            b.addSubBehaviour(new SendMessage1());
            b.addSubBehaviour(new SendMessage2());
            b.addSubBehaviour(new SendMessage3());
        }
        
         @Override
        protected void takeDown(){
            super.takeDown();
            System.out.println(this.getLocalName() + " dying!");
        }
        
        private class SendMessage1 extends SimpleBehaviour{
            
            private int counter=0;
            
            @Override
            public void action() {
                AID receiver = new AID();
                receiver.setLocalName("petri_name");

                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                long time = System.currentTimeMillis();
                msg.setConversationId(""+time);
                msg.addReceiver(receiver);

                if(time % 2 == 0){
                    msg.setContent("ping1");
                }
                else{
                    msg.setContent("not ping1");
                }

                myAgent.send(msg);

                counter++;

                System.out.println("Executing SendMessage1 for the " + counter + " time!");
            }

            @Override
            public boolean done() {
                if(counter >= 3){
                    System.out.println("Behaviour SendMessage1 ending!");
                    return true;
                }
                else return false;
            }
        }
        
        private class SendMessage2 extends SimpleBehaviour{
            
            private int counter=0;

            @Override
            public void action() {
                AID receiver = new AID();
                receiver.setLocalName("petri_name");

                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                long time = System.currentTimeMillis();
                msg.setConversationId(""+time);
                msg.addReceiver(receiver);

                if(time % 2 == 0){
                    msg.setContent("ping2");
                }
                else{
                    msg.setContent("not ping2");
                }

                myAgent.send(msg);

                counter++;

                System.out.println("Executing SendMessage2 for the " + counter + " time!");
            }

            @Override
            public boolean done() {
                if(counter >= 3){
                    System.out.println("Behaviour SendMessage2 ending!");
                    return true;
                }
                else return false;
            }
        }
        
        private class SendMessage3 extends SimpleBehaviour{
            
            private int counter=0;

            @Override
            public void action() {
                AID receiver = new AID();
                receiver.setLocalName("petri_name");

                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                long time = System.currentTimeMillis();
                msg.setConversationId(""+time);
                msg.addReceiver(receiver);

                if(time % 2 == 0){
                    msg.setContent("ping3");
                }
                else{
                    msg.setContent("not ping3");
                }

                myAgent.send(msg);

                counter++;

                System.out.println("Executing SendMessage3 for the " + counter + " time!");
            }

            @Override
            public boolean done() {
                if(counter >= 3){
                    System.out.println("Behaviour SendMessage3 ending!");
                    return true;
                }
                else return false;
            }
        }
}
