/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Agents_Example;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

public class Basic extends Agent {

    @Override
    protected void setup() {
        super.setup();
        System.out.println(this.getLocalName() + " starting!");

        //Behaviours
        //this.addBehaviour(new /*behaviourclass*/);
    }
    
    private class SendMessage extends TickerBehaviour {

        public SendMessage(Agent a, long period) {
            super(a, period);
        }

        @Override
        protected void onTick() {
            AID receiver = new AID();
            receiver.setLocalName("petri_name");
            long time = System.currentTimeMillis();
            
            if(time % 2 == 0){
                ACLMessage msg1 = new ACLMessage(ACLMessage.PROPOSE);
                msg1.setOntology("event");
                msg1.setContent("enviar mensagem de proposta");
                msg1.setConversationId(""+time);
                msg1.addReceiver(receiver);
                myAgent.send(msg1);
            }
            else{
                ACLMessage msg2 = new ACLMessage(ACLMessage.INFORM);
                msg2.setContent("enviar mensagem informativa");
                msg2.setConversationId(""+time);
                msg2.addReceiver(receiver);
                myAgent.send(msg2);
            }
            
            block(1000);
        }
    }

    @Override
    protected void takeDown() {
        super.takeDown();
        System.out.println(this.getLocalName() + " dying!");
    }

}
