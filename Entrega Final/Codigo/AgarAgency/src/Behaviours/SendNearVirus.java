package Behaviours;

import Data.Player;
import Data.Virus;
import Packages.NearVirus;
import Presentation.GameMaster;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.util.Iterator;

public class SendNearVirus extends TickerBehaviour{
    
    // ----- VARIABLES ----- //
    
            private GameMaster gm;
    
    // ----- CONSTRUCTORS ----- //
    
    public SendNearVirus(Agent a, long time, GameMaster m){
        super(a, time);
        this.gm = m;
    }
    
    // ----- METHODS ----- //

    @Override
    protected void onTick() {
        synchronized(this.gm.getTable().getPlayersPointer()){
            Iterator<String> it_players = this.gm.getTable().getPlayersPointer().keySet().iterator();
            while(it_players.hasNext()){
                
                String name = it_players.next();
                Player p = this.gm.getTable().getPlayersPointer().get(name);
                NearVirus n = new NearVirus();
                synchronized(this.gm.getTable().getVirusPointer()){
                    
                    // Calculate nearest Virus
                    Iterator<String> it_virus = this.gm.getTable().getVirusPointer().keySet().iterator();
                    while(it_virus.hasNext()){
                        String key = it_virus.next();
                        Virus v = this.gm.getTable().getVirusPointer().get(key);
                        if(p.sights(v, p.getRange())){
                            n.addNear(v);
                        }
                    }
                    
                }
                
                try{
                    // Set receiver
                    AID receiver = new AID();
                    receiver.setLocalName(name);
                    
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    
                    // Set message content
                    msg.addReceiver(receiver);
                    msg.setConversationId("21");
                    msg.setContentObject(n);

                    // Send message
                    myAgent.send(msg);
                }
                catch(IOException e){
                    System.out.println("[SERVICES] Service '" + myAgent.getLocalName() + "' couldn't send near virus!");
                }
                
            }
        }
        
    }
        
}
