Encontram aqui presentes dois grupos distintos: o sistema em si e os agentes. 
Para que a experi�ncia do jogo comece, deve ser gerado um Petri com virus e aglomerados colocados aleat�riamente pelo espa�o. 
Preparado o ambiente, s�o inseridas as c�lulas controladas pelos agentes. 
A intera��o no ambiente � feita circulando de um lado para o outro absorvendo aglomerados at� que c�lulas sejam detetadas no seu campo de vis�o. 
Quando detetado, � fornecida ao memso a informa��o do tamanho. Se for sufcientemente grande para ser uma amea�a, tenta fugir do seu campo de vis�o podendo ou n�o ser absorvido. 
Se for absorvido, � eliminado do Petri, se conseguir fugir ao campo de vis�o da c�lula, continua a circular. 
Caso a c�lula seja suficientemente pequena para a absorver, vai tentar alcan�a-la at� que consiga absorv�-la. 
Caso esta saia do seu campo de vis�o continua a circular. 
Se a c�lula tiver um tamanho aproximado, ignora. D
urante estas a��es poder�o surgir virus no qual o agente poder� aproveitar para sua defesa contra amea�as ou poder�o ser estas uma amea�a para a sua "performance" no ambiente. 
Todas estas a��es poder�o acontecer simultaneamente, tendo que o agente saber lidar com estas de forma a conseguir sobreviver no Petri. 
Esta intera��o encontra-se modelada na Figura 2.

>>>>>>[FIGURA 2]>>>>>>